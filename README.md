# Flywheel Exchange

This repository hosts the Flywheel Gear Exchange inventory which feed the Flywheel
makerting site and well as the gear library available to Flywheel CLI users.

It consists of two type of distinct assets:

- User-generated weakly-versioned *gears* manifest and machine-generated,
  strongly-versioned *manifests*
- Utility scripts (*maintanence* directory) and CI/CD scripts (*bin* directory) for
  managing the Exchange

Manifest processing code is automatically executed by Gitlab CI whenever a new commit
gets pushed. It validates weakly-versioned *gears* and converts them to
strongly-versioned *manifests* and *rootfs* build artifacts.

## Usage

To add a new gear to the Exchange, or to update an existing one, follow these steps:

- Create a properly-formatted [gear manifest](https://gitlab.com/flywheel-io/public/gears/-/tree/master/spec)
- Optionally, publish the gear's source code, e.g., on GitHub, Gilab
- Publish a tagged Docker image of the gear, e.g., on Docker Hub
- Your manifest must reference the Docker image location and tag under `custom.gear-builder.image`
- Open a pull request against this repo that adds your manifest to `gears/<org>/<gear-name>`

## Operation

The publication of the gear to the Gear-Exchange is handled by the CI/CD pipeline.

## Maintenance

Additionals scripts are provided in the *maintenance* directory to assist in the
process of administrating the Gear-Exchange. See the [README](maintenance/README.md)
for more information.
