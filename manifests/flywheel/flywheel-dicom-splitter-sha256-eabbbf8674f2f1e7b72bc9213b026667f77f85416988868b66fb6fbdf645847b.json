{
  "exchange": {
    "git-commit": "89e246bf67c71fce8e638198e2392e8214c1e91e",
    "rootfs-hash": "sha256:eabbbf8674f2f1e7b72bc9213b026667f77f85416988868b66fb6fbdf645847b",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-dicom-splitter:2.0.0"
  },
  "gear": {
    "author": "Flywheel",
    "cite": "",
    "command": "python run.py",
    "config": {
      "debug": {
        "default": false,
        "description": "Include debug output",
        "type": "boolean"
      },
      "delete_input": {
        "default": true,
        "description": "Delete input on successful split. Default true.",
        "type": "boolean"
      },
      "extract_localizer": {
        "default": true,
        "description": "If true and DICOM archive contains embedded localizer images (ImageType = Localizer), the embedded images will be saved as their own DICOM archive",
        "type": "boolean"
      },
      "group_by": {
        "default": "SeriesInstanceUID",
        "description": "Comma-separated tags to group dicom frames by",
        "type": "string"
      },
      "max_geometric_splits": {
        "default": 4,
        "description": "Maximum number of splits to perform by image orientation and/or position",
        "type": "integer"
      },
      "tag": {
        "default": "dicom-splitter",
        "description": "The tag to be added to files upon run completion",
        "type": "string"
      },
      "tag-single-output": {
        "default": "",
        "description": "In addition to the tag applied to all files above, apply a second tag to a single output so that a downstream gear rule can run on the acquisition once splitter finishes.  Default empty, no tag will be applied",
        "type": "string"
      },
      "zip-single-dicom": {
        "default": "match",
        "description": "Zip single dicom outputs",
        "enum": [
          "no",
          "match"
        ],
        "type": "string"
      }
    },
    "custom": {
      "flywheel": {
        "classification": {
          "function": [
            "Curation"
          ],
          "modality": [
            "Any"
          ],
          "organ": [
            "Other"
          ],
          "species": [
            "Phantom",
            "Human",
            "Animal",
            "Other"
          ],
          "therapeutic_area": [
            "Any"
          ]
        },
        "show-job": true,
        "suite": "Curation"
      },
      "gear-builder": {
        "category": "converter",
        "image": "flywheel/dicom-splitter:2.0.0"
      }
    },
    "description": "The DICOM splitter extracts embedded localizer DICOM frames and/or re-group DICOM frames in archive by specific DICOM tags provided by user.",
    "environment": {
      "COMMIT": "master.3a9476be",
      "COMMIT_TIME": "2021-10-25T09:24:15+00:00",
      "EDITOR": "micro",
      "FLYWHEEL": "/flywheel/v0",
      "FW_DCM_READING_VALIDATION_MODE": "1",
      "FW_DCM_READ_ONLY": "false",
      "FW_DCM_WRITING_VALIDATION_MODE": "2",
      "GDCM_VERSION": "3.0.9",
      "GJO_VERSION": "1.0.2",
      "GPG_KEY": "E3FF2839C048B25C084DEBE9B26995E310250568",
      "JQ_VERSION": "jq-1.6",
      "LANG": "C.UTF-8",
      "MICRO_VERSION": "2.0.10",
      "MUSTACHE_VERSION": "1.3.0",
      "PATH": "/opt/poetry/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
      "PIP_NO_CACHE_DIR": "0",
      "POETRY_HOME": "/opt/poetry",
      "POETRY_VERSION": "1.1.11",
      "POETRY_VIRTUALENVS_CREATE": "false",
      "PROMETHEUS_MULTIPROC_DIR": "/var/metrics",
      "PWD": "/flywheel/v0",
      "PYTHON_GET_PIP_SHA256": "01249aa3e58ffb3e1686b7141b4e9aac4d398ef4ac3012ed9dff8dd9f685ffe0",
      "PYTHON_GET_PIP_URL": "https://github.com/pypa/get-pip/raw/d781367b97acf0ece7e9e304bf281e99b618bf10/public/get-pip.py",
      "PYTHON_PIP_VERSION": "21.2.4",
      "PYTHON_SETUPTOOLS_VERSION": "57.5.0",
      "PYTHON_VERSION": "3.9.7"
    },
    "inputs": {
      "api-key": {
        "base": "api-key",
        "read-only": false
      },
      "dicom": {
        "base": "file",
        "description": "Dicom Archive",
        "optional": false,
        "type": {
          "enum": [
            "dicom"
          ]
        }
      }
    },
    "label": "DICOM splitter",
    "license": "MIT",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "dicom-splitter",
    "source": "https://gitlab.com/flywheel-io/scientific-solutions/gears/dicom-splitter",
    "url": "https://gitlab.com/flywheel-io/scientific-solutions/gears/dicom-splitter",
    "version": "2.0.0"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "debug": {
            "default": false,
            "type": "boolean"
          },
          "delete_input": {
            "default": true,
            "type": "boolean"
          },
          "extract_localizer": {
            "default": true,
            "type": "boolean"
          },
          "group_by": {
            "default": "SeriesInstanceUID",
            "type": "string"
          },
          "max_geometric_splits": {
            "default": 4,
            "type": "integer"
          },
          "tag": {
            "default": "dicom-splitter",
            "type": "string"
          },
          "tag-single-output": {
            "default": "",
            "type": "string"
          },
          "zip-single-dicom": {
            "default": "match",
            "enum": [
              "no",
              "match"
            ],
            "type": "string"
          }
        },
        "required": [
          "debug",
          "delete_input",
          "extract_localizer",
          "group_by",
          "max_geometric_splits",
          "tag",
          "tag-single-output",
          "zip-single-dicom"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "api-key": {
            "type": "object"
          },
          "dicom": {
            "properties": {
              "type": {
                "enum": [
                  "dicom"
                ]
              }
            },
            "type": "object"
          }
        },
        "required": [
          "api-key",
          "dicom"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for DICOM splitter",
    "type": "object"
  }
}
