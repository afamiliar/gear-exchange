{
  "exchange": {
    "git-commit": "17ed6182e904c774d9a001a57e2d391f53612be4",
    "rootfs-hash": "sha256:e923ffe2a9fa36d4dfcf2bfac7c34b57fd9c711e47d9b75905f81c5feccc6173",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-dicom-send:4.1.1"
  },
  "gear": {
    "author": "Flywheel",
    "command": "poetry run python run.py",
    "config": {
      "called_ae": {
        "description": "The Called AE title of the receiving DICOM server.",
        "type": "string"
      },
      "calling_ae": {
        "default": "flywheel",
        "description": "The Calling AE title. Default = flywheel",
        "type": "string"
      },
      "debug": {
        "default": false,
        "description": "Enable debug logging",
        "type": "boolean"
      },
      "destination": {
        "description": "The IP address or hostname of the destination DICOM server. Note: The server must be reachable from the engine host of the Flywheel instance.",
        "type": "string"
      },
      "file_download_retry_time": {
        "default": 60,
        "description": "Max retry time per file when backing-off on file download errors. Defaults to 1 minute (60s)",
        "type": "number"
      },
      "group": {
        "default": "0x0021",
        "description": "Dicom group to apply dicom-send tag",
        "type": "string"
      },
      "identifier": {
        "default": "Flywheel",
        "description": "Private tag creator for dicom-send tag",
        "type": "string"
      },
      "port": {
        "default": 104,
        "description": "Port number of the listening DICOM service. Default = 104",
        "type": "number"
      },
      "report": {
        "default": true,
        "description": "Save a report of the transmission to the destination container",
        "type": "boolean"
      },
      "tag_value": {
        "default": "DICOM send",
        "description": "Dicom tag value for dicom-send tag",
        "type": "string"
      },
      "tls_enabled": {
        "default": false,
        "description": "Enable or disable TLS normal or anonymous",
        "type": "boolean"
      },
      "tls_require_peer_cert": {
        "default": true,
        "description": "Verify peer certificate, fail if absent",
        "type": "boolean"
      }
    },
    "custom": {
      "flywheel": {
        "classification": {
          "function": [
            "Export"
          ],
          "modality": [
            "Any"
          ],
          "organ": [
            "Any"
          ],
          "species": [
            "Phantom",
            "Human",
            "Animal",
            "Other"
          ],
          "therapeutic_area": [
            "Any"
          ]
        },
        "suite": "Export"
      },
      "gear-builder": {
        "category": "utility",
        "image": "flywheel/dicom-send:4.1.1"
      }
    },
    "description": "DICOM Send utilizes DCMTK's storescu to send DICOM data from a Flywheel instance to a destination DICOM server, hosted externally. This Gear supports the transmission of individual DICOM files and archives, as well as the transmission of an entire session when a specific input is not provided. Note that a private tag is added to each DICOM file to be transmitted (Flywheel:DICOM Send, at group 0x0021). Importantly, the external DICOM server must be reachable from the engine host of the Flywheel instance.",
    "environment": {
      "COMMIT": "main.66a70563",
      "COMMIT_TIME": "2022-04-04T09:04:44+00:00",
      "EDITOR": "micro",
      "FLYWHEEL": "/flywheel/v0",
      "GJO_VERSION": "1.0.2",
      "GPG_KEY": "A035C8C19219BA821ECEA86B64E628F8D684696D",
      "JQ_VERSION": "jq-1.6",
      "LANG": "C.UTF-8",
      "MICRO_VERSION": "2.0.10",
      "MUSTACHE_VERSION": "1.3.1",
      "PATH": "/opt/poetry/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
      "PIP_NO_CACHE_DIR": "0",
      "POETRY_HOME": "/opt/poetry",
      "POETRY_VERSION": "1.1.13",
      "POETRY_VIRTUALENVS_CREATE": "false",
      "PROMETHEUS_MULTIPROC_DIR": "/var/metrics",
      "PWD": "/flywheel/v0",
      "PYSITE": "/usr/local/lib/python3.10/site-packages",
      "PYTHON_GET_PIP_SHA256": "e235c437e5c7d7524fbce3880ca39b917a73dc565e0c813465b7a7a329bb279a",
      "PYTHON_GET_PIP_URL": "https://github.com/pypa/get-pip/raw/38e54e5de07c66e875c11a1ebbdb938854625dd8/public/get-pip.py",
      "PYTHON_PIP_VERSION": "22.0.4",
      "PYTHON_SETUPTOOLS_VERSION": "58.1.0",
      "PYTHON_VERSION": "3.10.4",
      "SETUPTOOLS_USE_DISTUTILS": "stdlib"
    },
    "inputs": {
      "add_cert_file": {
        "base": "file",
        "description": "Optional Cert file to add to list of certificates",
        "optional": true
      },
      "api-key": {
        "base": "api-key",
        "read-only": false
      },
      "cert": {
        "base": "file",
        "description": "TLS cert.  Needed if tls is enabled.",
        "optional": true
      },
      "file": {
        "base": "file",
        "description": "Any DICOM file or an archive (zip or tar) containing DICOM file(s). Non DICOM files are ignored. If no input is provided, all DICOM files in the session where the Gear is executed are downloaded and used as input.",
        "optional": true,
        "type": {
          "enum": [
            "dicom"
          ]
        }
      },
      "key": {
        "base": "file",
        "description": "TLS key.  Needed if tls is enabled",
        "optional": true
      }
    },
    "label": "DICOM Send",
    "license": "Other",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "dicom-send",
    "source": "http://support.dcmtk.org/docs/storescu.html",
    "url": "https://gitlab.com/flywheel-io/flywheel-apps/dicom-send",
    "version": "4.1.1"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "called_ae": {
            "type": "string"
          },
          "calling_ae": {
            "default": "flywheel",
            "type": "string"
          },
          "debug": {
            "default": false,
            "type": "boolean"
          },
          "destination": {
            "type": "string"
          },
          "file_download_retry_time": {
            "default": 60,
            "type": "number"
          },
          "group": {
            "default": "0x0021",
            "type": "string"
          },
          "identifier": {
            "default": "Flywheel",
            "type": "string"
          },
          "port": {
            "default": 104,
            "type": "number"
          },
          "report": {
            "default": true,
            "type": "boolean"
          },
          "tag_value": {
            "default": "DICOM send",
            "type": "string"
          },
          "tls_enabled": {
            "default": false,
            "type": "boolean"
          },
          "tls_require_peer_cert": {
            "default": true,
            "type": "boolean"
          }
        },
        "required": [
          "called_ae",
          "calling_ae",
          "debug",
          "destination",
          "file_download_retry_time",
          "group",
          "identifier",
          "port",
          "report",
          "tag_value",
          "tls_enabled",
          "tls_require_peer_cert"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "add_cert_file": {
            "properties": {},
            "type": "object"
          },
          "api-key": {
            "type": "object"
          },
          "cert": {
            "properties": {},
            "type": "object"
          },
          "file": {
            "properties": {
              "type": {
                "enum": [
                  "dicom"
                ]
              }
            },
            "type": "object"
          },
          "key": {
            "properties": {},
            "type": "object"
          }
        },
        "required": [
          "api-key"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for DICOM Send",
    "type": "object"
  }
}
