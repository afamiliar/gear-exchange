{
  "exchange": {
    "git-commit": "d4fd1a41b5080d05aab051626b74c2445ff77ef7",
    "rootfs-hash": "sha256:9745dccee3ec0527457b6a7149f360c2f44647b23b8622c1c63cc6c5192a2c44",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-mideface:0.1.3_7.3.2"
  },
  "gear": {
    "author": "Flywheel",
    "cite": "https://surfer.nmr.mgh.harvard.edu/fswiki/FreeSurferMethodsCitation",
    "command": "python run.py",
    "config": {
      "back_of_head": {
        "default": false,
        "description": "Include back of head in the defacing",
        "type": "boolean"
      },
      "code": {
        "description": "Embed codename in pics",
        "optional": true,
        "type": "string"
      },
      "copy_input_metadata_to_output": {
        "default": true,
        "description": "Copy the input file's metadata to the output file",
        "type": "boolean"
      },
      "debug": {
        "default": false,
        "description": "Log debug messages",
        "type": "boolean"
      },
      "display": {
        "description": "Set Xvfb display no for taking pics",
        "optional": true,
        "type": "integer"
      },
      "expert": {
        "description": "Set expert options",
        "optional": true,
        "type": "string"
      },
      "fill_const": {
        "description": "Fill constraints: constIn constOut",
        "optional": true,
        "type": "string"
      },
      "fill_zero": {
        "default": false,
        "description": "Fill with zeroes",
        "type": "boolean"
      },
      "forehead": {
        "default": false,
        "description": "Include forehead in defacing (risks removing brain)",
        "type": "boolean"
      },
      "function": {
        "default": "infile",
        "description": "Defacing function to utilize, 'infile' or 'apply'",
        "enum": [
          "infile",
          "apply"
        ],
        "type": "string"
      },
      "keep_facemask": {
        "default": false,
        "description": "Retain face.mask.mgz file",
        "type": "boolean"
      },
      "no_ears": {
        "default": false,
        "description": "Do not include ears in the defacing",
        "type": "boolean"
      },
      "no_post": {
        "default": false,
        "description": "Do not make a head surface after defacing",
        "type": "boolean"
      },
      "no_samseg_fast": {
        "default": false,
        "description": "Do NOT configure samseg to run quickly",
        "type": "boolean"
      },
      "output_filetype": {
        "default": "nii.gz",
        "description": "Set defaced output filetype, options 'nii.gz', 'nii', 'mgz', default 'nii.gz'",
        "enum": [
          "nii.gz",
          "nii",
          "mgz"
        ],
        "type": "string"
      },
      "pics": {
        "default": false,
        "description": "For QA: Take pictures, saved as codename.face-before.png, codename.face-after.png, and codename.before+after.gif in the output directory.",
        "type": "boolean"
      },
      "samseg_fast": {
        "default": false,
        "description": "Configure samseg to run quickly; sets ndil=1 (default)",
        "type": "boolean"
      },
      "save_outputs": {
        "default": false,
        "description": "Save all additional output from MiDeface as a zip file",
        "type": "boolean"
      },
      "tag": {
        "default": "mideface",
        "description": "The tag to be added to output files upon run completion",
        "type": "string"
      },
      "threads": {
        "description": "Number of CPUs to use",
        "optional": true,
        "type": "integer"
      },
      "xmask_samseg": {
        "description": "Segment input using samseg (14GB, +~20-40min)",
        "optional": true,
        "type": "integer"
      },
      "xmask_synthseg": {
        "description": "Segment input using synthseg (35GB, +~20min)",
        "optional": true,
        "type": "integer"
      }
    },
    "custom": {
      "flywheel": {
        "classification": {
          "function": [
            "Image Processing - Other"
          ],
          "modality": [
            "MR"
          ],
          "organ": [
            "Brain"
          ],
          "species": [
            "Human"
          ],
          "therapeutic_area": [
            "Neurology",
            "Psychiatry/Psychology"
          ]
        },
        "show-job": true,
        "suite": "Image Processing"
      },
      "gear-builder": {
        "category": "converter",
        "image": "flywheel/mideface:0.1.3_7.3.2"
      }
    },
    "description": "Flywheel gear for the implementation of FreeSurfer's MiDeFace to remove facial structures from MRI images.",
    "environment": {
      "FLYWHEEL": "/flywheel/v0",
      "FREESURFER": "/usr/local/freesurfer",
      "FREESURFER_HOME": "/usr/local/freesurfer",
      "FSL_OUTPUT_FORMAT": "nii.gz",
      "FS_LICENSE": "/usr/local/freesurfer/license.txt",
      "FS_OVERRIDE": "0",
      "GPG_KEY": "A035C8C19219BA821ECEA86B64E628F8D684696D",
      "LANG": "C.UTF-8",
      "LD_LIBRARY_PATH": "/usr/local/ImageMagick-7.1.1/lib:/usr/local/lib",
      "MAGICK_HOME": "/usr/local/ImageMagick-7.1.1",
      "OS": "Linux",
      "PATH": "/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/freesurfer/bin:/usr/local/freesurfer/fsfast/bin:/usr/local/ImageMagick-7.1.1/bin",
      "PWD": "/flywheel/v0",
      "PYTHON_GET_PIP_SHA256": "394be00f13fa1b9aaa47e911bdb59a09c3b2986472130f30aa0bfaf7f3980637",
      "PYTHON_GET_PIP_URL": "https://github.com/pypa/get-pip/raw/d5cb0afaf23b8520f1bbcfed521017b4a95f5c01/public/get-pip.py",
      "PYTHON_PIP_VERSION": "22.3.1",
      "PYTHON_SETUPTOOLS_VERSION": "65.5.1",
      "PYTHON_VERSION": "3.11.3",
      "SUBJECTS_DIR": "/usr/local/freesurfer/subjects"
    },
    "inputs": {
      "api-key": {
        "base": "api-key",
        "read-only": false
      },
      "apply_facemask": {
        "base": "file",
        "description": "If using apply function, facemask.mgz to apply",
        "optional": true
      },
      "apply_reg": {
        "base": "file",
        "description": "If using apply function, reg.lta file to apply",
        "optional": true
      },
      "freesurfer_license_file": {
        "base": "file",
        "description": "Freesurfer license file. Obtaining a license is free and can be obtained at surfer.nmr.mgh.harvard.edu/fswiki/License",
        "optional": true
      },
      "infile": {
        "base": "file",
        "description": "Input to deface, in NIfTI or mgz format",
        "optional": false,
        "type": {
          "enum": [
            "nifti",
            "mgz"
          ]
        }
      },
      "init_reg": {
        "base": "file",
        "description": "If using infile function, initialize samseg with registration.lta",
        "optional": true
      },
      "samseg_json": {
        "base": "file",
        "description": "If using infile function, configure samseg with json file",
        "optional": true,
        "type": {
          "enum": [
            "json"
          ]
        }
      },
      "xmask": {
        "base": "file",
        "description": "If using infile function, specify exclusion mask",
        "optional": true
      }
    },
    "label": "MiDeface",
    "license": "Other",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "mideface",
    "source": "https://surfer.nmr.mgh.harvard.edu/fswiki/MiDeFace",
    "url": "https://gitlab.com/flywheel-io/scientific-solutions/gears/mideface",
    "version": "0.1.3_7.3.2"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "back_of_head": {
            "default": false,
            "type": "boolean"
          },
          "code": {
            "type": "string"
          },
          "copy_input_metadata_to_output": {
            "default": true,
            "type": "boolean"
          },
          "debug": {
            "default": false,
            "type": "boolean"
          },
          "display": {
            "type": "integer"
          },
          "expert": {
            "type": "string"
          },
          "fill_const": {
            "type": "string"
          },
          "fill_zero": {
            "default": false,
            "type": "boolean"
          },
          "forehead": {
            "default": false,
            "type": "boolean"
          },
          "function": {
            "default": "infile",
            "enum": [
              "infile",
              "apply"
            ],
            "type": "string"
          },
          "keep_facemask": {
            "default": false,
            "type": "boolean"
          },
          "no_ears": {
            "default": false,
            "type": "boolean"
          },
          "no_post": {
            "default": false,
            "type": "boolean"
          },
          "no_samseg_fast": {
            "default": false,
            "type": "boolean"
          },
          "output_filetype": {
            "default": "nii.gz",
            "enum": [
              "nii.gz",
              "nii",
              "mgz"
            ],
            "type": "string"
          },
          "pics": {
            "default": false,
            "type": "boolean"
          },
          "samseg_fast": {
            "default": false,
            "type": "boolean"
          },
          "save_outputs": {
            "default": false,
            "type": "boolean"
          },
          "tag": {
            "default": "mideface",
            "type": "string"
          },
          "threads": {
            "type": "integer"
          },
          "xmask_samseg": {
            "type": "integer"
          },
          "xmask_synthseg": {
            "type": "integer"
          }
        },
        "required": [
          "back_of_head",
          "copy_input_metadata_to_output",
          "debug",
          "fill_zero",
          "forehead",
          "function",
          "keep_facemask",
          "no_ears",
          "no_post",
          "no_samseg_fast",
          "output_filetype",
          "pics",
          "samseg_fast",
          "save_outputs",
          "tag"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "api-key": {
            "type": "object"
          },
          "apply_facemask": {
            "properties": {},
            "type": "object"
          },
          "apply_reg": {
            "properties": {},
            "type": "object"
          },
          "freesurfer_license_file": {
            "properties": {},
            "type": "object"
          },
          "infile": {
            "properties": {
              "type": {
                "enum": [
                  "nifti",
                  "mgz"
                ]
              }
            },
            "type": "object"
          },
          "init_reg": {
            "properties": {},
            "type": "object"
          },
          "samseg_json": {
            "properties": {
              "type": {
                "enum": [
                  "json"
                ]
              }
            },
            "type": "object"
          },
          "xmask": {
            "properties": {},
            "type": "object"
          }
        },
        "required": [
          "api-key",
          "infile"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for MiDeface",
    "type": "object"
  }
}
