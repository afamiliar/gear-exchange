{
  "exchange": {
    "git-commit": "a05b13ce3abadd9ebf2ce490120f63bb40a05013",
    "rootfs-hash": "sha256:3b79f03ffbe8343b9651e9b44c80336ee7a401e44ae4d89a690bdbc043d20561",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-dicom-fixer:0.10.6"
  },
  "gear": {
    "author": "Flywheel",
    "cite": "",
    "command": "python run.py",
    "config": {
      "convert-palette": {
        "default": true,
        "description": "Convert palette color images to RGB, this may not work for some modalities, please see gear readme",
        "type": "boolean"
      },
      "debug": {
        "default": false,
        "description": "Log debug messages",
        "type": "boolean"
      },
      "dicom-standard": {
        "default": "local",
        "description": "Specify which DICOM standards revision to use. Default `local` (faster), `current` optional for most up-to-date revision.",
        "enum": [
          "local",
          "current"
        ],
        "type": "string"
      },
      "force_decompress": {
        "default": false,
        "description": "Expert option: Force standardize_transfer_syntax fix even if filesize may be too big for available memory. WARNING: Choosing this option may result in the gear failing due to being out of memory.",
        "type": "boolean"
      },
      "new-uids-needed": {
        "default": false,
        "description": "Create new SeriesInstanceUID and StudyInstanceUID for each dicom.",
        "type": "boolean"
      },
      "pixel-data-check": {
        "default": true,
        "description": "Check that pixel data is parsable.",
        "type": "boolean"
      },
      "standardize_transfer_syntax": {
        "default": true,
        "description": "Decompress dicom's if possible and adjust TransferSyntaxUID to ExplicitVRLittleEndian",
        "type": "boolean"
      },
      "strict-validation": {
        "default": true,
        "description": "Enforce strict DICOM validation if true; else, allow python-parsable values that may not meet DICOM standards.",
        "type": "boolean"
      },
      "tag": {
        "default": "dicom-fixer",
        "description": "The tag to be added on input file upon run completion.",
        "type": "string"
      },
      "unique": {
        "default": true,
        "description": "Enforce dicom uniqueness by SOPInstanceUID or file hash. Remove duplicate dicoms",
        "type": "boolean"
      },
      "zip-single-dicom": {
        "default": "match",
        "description": "Output a single dicom as zip (.dcm.zip) instead of as a dicom (.dcm) or match input.",
        "enum": [
          "match",
          "yes",
          "no"
        ],
        "type": "string"
      }
    },
    "custom": {
      "flywheel": {
        "classification": {
          "function": [
            "Curation"
          ],
          "modality": [
            "Any"
          ],
          "organ": [
            "Any"
          ],
          "species": [
            "Phantom",
            "Human",
            "Animal",
            "Other"
          ],
          "therapeutic_area": [
            "Any"
          ]
        },
        "show-job": true,
        "suite": "Curation"
      },
      "gear-builder": {
        "category": "converter",
        "image": "flywheel/dicom-fixer:0.10.6"
      }
    },
    "description": "Fixes various issues in dicoms.",
    "environment": {
      "BUILD_TIME": "2024-11-19T14:11:49Z",
      "CI_JOB_URL": "https://gitlab.com/flywheel-io/tools/img/python/-/jobs/8412894086",
      "COMMIT": "master.3a9476be",
      "COMMIT_REF": "main",
      "COMMIT_SHA": "a02878f2",
      "COMMIT_TAG": "",
      "COMMIT_TIME": "2021-10-25T09:24:15+00:00",
      "EDITOR": "micro",
      "EGET_BIN": "/opt/bin",
      "FLYWHEEL": "/flywheel/v0",
      "GDCM_VERSION": "3.0.9",
      "GJO_VERSION": "1.0.2",
      "GPG_KEY": "7169605F62C751356D054A26A821E680E5FA6305",
      "HOME": "/root",
      "HOSTNAME": "9623bfc6c9b5",
      "JQ_VERSION": "jq-1.6",
      "LANG": "C.UTF-8",
      "MICRO_VERSION": "2.0.10",
      "MUSTACHE_VERSION": "1.3.0",
      "PATH": "/venv/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/bin",
      "PIP_NO_CACHE_DIR": "0",
      "POETRY_HOME": "/opt/poetry",
      "POETRY_VERSION": "1.1.11",
      "POETRY_VIRTUALENVS_CREATE": "false",
      "PROMETHEUS_MULTIPROC_DIR": "/var/metrics",
      "PWD": "/flywheel/v0",
      "PYSITE": "/usr/local/lib/python3.11/site-packages",
      "PYTHONPATH": "/venv/lib/python/site-packages",
      "PYTHON_GET_PIP_SHA256": "394be00f13fa1b9aaa47e911bdb59a09c3b2986472130f30aa0bfaf7f3980637",
      "PYTHON_GET_PIP_URL": "https://github.com/pypa/get-pip/raw/d5cb0afaf23b8520f1bbcfed521017b4a95f5c01/public/get-pip.py",
      "PYTHON_PIP_VERSION": "22.3.1",
      "PYTHON_SETUPTOOLS_VERSION": "65.5.1",
      "PYTHON_SHA256": "086de5882e3cb310d4dca48457522e2e48018ecd43da9cdf827f6a0759efb07d",
      "PYTHON_VERSION": "3.13.0",
      "SETUPTOOLS_USE_DISTUTILS": "stdlib",
      "SHLVL": "1",
      "TERM": "xterm",
      "TZ": "UTC",
      "UV_NO_CACHE": "1",
      "VIRTUAL_ENV": "/venv"
    },
    "inputs": {
      "api-key": {
        "base": "api-key",
        "read-only": false
      },
      "dicom": {
        "base": "file",
        "description": "Input dicom.",
        "optional": false,
        "type": {
          "enum": [
            "dicom"
          ]
        }
      }
    },
    "label": "Dicom Fixer",
    "license": "MIT",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "dicom-fixer",
    "output_configuration": {
      "enforce_file_version_match": true
    },
    "source": "https://gitlab.com/flywheel-io/scientific-solutions/gears/dicom-fixer",
    "url": "https://gitlab.com/flywheel-io/scientific-solutions/gears/dicom-fixer",
    "version": "0.10.6"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "convert-palette": {
            "default": true,
            "type": "boolean"
          },
          "debug": {
            "default": false,
            "type": "boolean"
          },
          "dicom-standard": {
            "default": "local",
            "enum": [
              "local",
              "current"
            ],
            "type": "string"
          },
          "force_decompress": {
            "default": false,
            "type": "boolean"
          },
          "new-uids-needed": {
            "default": false,
            "type": "boolean"
          },
          "pixel-data-check": {
            "default": true,
            "type": "boolean"
          },
          "standardize_transfer_syntax": {
            "default": true,
            "type": "boolean"
          },
          "strict-validation": {
            "default": true,
            "type": "boolean"
          },
          "tag": {
            "default": "dicom-fixer",
            "type": "string"
          },
          "unique": {
            "default": true,
            "type": "boolean"
          },
          "zip-single-dicom": {
            "default": "match",
            "enum": [
              "match",
              "yes",
              "no"
            ],
            "type": "string"
          }
        },
        "required": [
          "convert-palette",
          "debug",
          "dicom-standard",
          "force_decompress",
          "new-uids-needed",
          "pixel-data-check",
          "standardize_transfer_syntax",
          "strict-validation",
          "tag",
          "unique",
          "zip-single-dicom"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "api-key": {
            "type": "object"
          },
          "dicom": {
            "properties": {
              "type": {
                "enum": [
                  "dicom"
                ]
              }
            },
            "type": "object"
          }
        },
        "required": [
          "api-key",
          "dicom"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for Dicom Fixer",
    "type": "object"
  }
}
