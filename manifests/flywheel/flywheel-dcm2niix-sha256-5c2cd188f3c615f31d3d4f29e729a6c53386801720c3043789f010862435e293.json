{
  "exchange": {
    "git-commit": "ad3b6846b87f701878ee8d735309f69698985002",
    "rootfs-hash": "sha256:5c2cd188f3c615f31d3d4f29e729a6c53386801720c3043789f010862435e293",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-dcm2niix:2.1.2_1.0.20240202"
  },
  "gear": {
    "author": "Flywheel",
    "cite": "dcm2niix doi: 10.1016/j.jneumeth.2016.03.001",
    "command": "python /flywheel/v0/run.py",
    "config": {
      "anonymize_bids": {
        "default": true,
        "description": "Anonymize BIDS. Options: true (default), false. 'bids_sidecar' config option must be enabled (i.e., 'y' or 'o' options).",
        "type": "boolean"
      },
      "bids_sidecar": {
        "default": "y",
        "description": "Output BIDS sidecar in JSON format. Options are 'y'=yes (default), 'n'=no, 'o'=only (whereby no NIfTI file will be generated). Note: bids_sidecar is always invoked when running dcm2niix to be used as metadata. User configuration preference is handled after acquiring metadata.",
        "enum": [
          "y",
          "n",
          "o"
        ],
        "type": "string"
      },
      "coil_combine": {
        "default": false,
        "description": "For sequences with individual coil data, saved as individual volumes, this option will save a NIfTI file with ONLY the combined coil data (i.e., the last volume). Options: true, false (default). WARNING: Expert Option. We make no effort to check for independent coil data; we trust that you know what you are asking for if you have selected this option.",
        "type": "boolean"
      },
      "comment": {
        "default": "",
        "description": "If non-empty, store comment as NIfTI aux_file. Options: non-empty string, 24 characters maximum. Note: The 24 character comment is placed in (1) all NIfTI output files in the aux_file variable. You can use fslhdr to access the NIfTI header data and see this comment; and (2) all JSON files (i.e., BIDS sidecars), which means the comment is stored as metadata for all associated output files and would be included in the 'bids_sidecar' file, if invoked.",
        "type": "string"
      },
      "compress_images": {
        "default": "y",
        "description": "Gzip compress images. Options: 'y'=yes (default), 'i'=internal, 'n'=no, '3'=no,3D. Note: If option '3' is chosen, the filename flag will be set to '-f %p_%s' to prevent overwriting files. Tip: If desire .nrrd output, select 'n'.",
        "enum": [
          "y",
          "i",
          "n",
          "3"
        ],
        "type": "string"
      },
      "compression_level": {
        "default": 6,
        "description": "Set the gz compression level. Options: 1 (fastest) to 9 (smallest), 6 (default).",
        "maximum": 9,
        "minimum": 1,
        "type": "number"
      },
      "convert_only_series": {
        "default": "all",
        "description": "Selectively convert by series number - can be used up to 16 times. Options: 'all' (default), space-separated list of series numbers (e.g., '2 12 20'). WARNING: Expert Option. We trust that if you have selected this option, you know what you are asking for.",
        "type": "string"
      },
      "crop": {
        "default": false,
        "description": "Crop 3D T1 images. Options: true, false (default).",
        "type": "boolean"
      },
      "dcm2niix_verbose": {
        "default": false,
        "description": "Whether to include verbose output from dcm2niix call. Options: true, false (default).",
        "type": "boolean"
      },
      "debug": {
        "default": false,
        "description": "Log debug messages",
        "type": "boolean"
      },
      "decompress_dicoms": {
        "default": false,
        "description": "Decompress DICOM files before conversion. This will perform decompression using gdcmconv and then perform the conversion using dcm2niix. Options: true, false (default).",
        "type": "boolean"
      },
      "filename": {
        "default": "%f_%p_%t_%s",
        "description": "Output filename template. Options: %a=antenna (coil) name, %b=basename, %c=comments, %d=series description, %e=echo number, %f=folder name, %i=ID of patient, %j=seriesInstanceUID, %k=studyInstanceUID, %m=manufacturer, %n=name of patient, %o=mediaObjectInstanceUID, %p=protocol, %r=instance number, %s=series number, %t=time, %u=acquisition number, %v=vendor, %x=study ID, %z=sequence name tag(0018,0024), %q sequence name tag(0018,1020). Defaults: dcm2niix tool `%f_%p_%t_%s`. Tip: A more informative filename can be useful for downstream BIDS curation by simply accessing relevant information in the extracted filename. For example, including echo number or protocol.",
        "type": "string"
      },
      "ignore_derived": {
        "default": false,
        "description": "Ignore derived, localizer, and 2D images. Options: true, false (default).",
        "type": "boolean"
      },
      "ignore_errors": {
        "default": false,
        "description": "Ignore dcm2niix errors and the exit status, and preserve outputs. Options: true, false (default). By default, when dcm2niix exits non-zero, outputs are not preserved. WARNING: Expert Option. We trust that if you have selected this option, you know what you are asking for.",
        "type": "boolean"
      },
      "lossless_scaling": {
        "default": "o",
        "description": "Losslessly scale 16-bit integers to use dynamic range. Options: 'y'=scale, 'n'=no, but unit16->int16, 'o'=original (default).",
        "enum": [
          "y",
          "n",
          "o"
        ],
        "type": "string"
      },
      "merge2d": {
        "default": 2,
        "description": "Merge 2D slices from the same series regardless of study time, echo, coil, orientation, etc. Options: 0=no, 1=yes, 2=auto (default)",
        "enum": [
          0,
          1,
          2
        ],
        "type": "number"
      },
      "output_nrrd": {
        "default": false,
        "description": "Export as NRRD instead of NIfTI. Options: true, false (default). Tip: To export .nrrd, change the **compress_images** config option to 'n'; otherwise, the output will split into two files (.raw.gz and .nhdr).",
        "type": "boolean"
      },
      "philips_scaling": {
        "default": true,
        "description": "Philips precise float (not display) scaling. Options: true (default), false.",
        "type": "boolean"
      },
      "remove_incomplete_volumes": {
        "default": false,
        "description": "Remove incomplete trailing volumes for 4D scans aborted mid-acquisition before dcm2niix conversion. Options: true, false (default).",
        "type": "boolean"
      },
      "sanitize_filename": {
        "default": true,
        "description": "Sanitize filename by removing invalid characters.",
        "type": "boolean"
      },
      "save_sidecar_as_metadata": {
        "default": "",
        "description": "Save sidecar information in `file.info` metadata to be compatible with previous BIDS workflow. Options: 'y'=yes, 'n'=no, or leave blank (default). If left blank, gear will check `project.info` and attempt to identify if the previous BIDS curation method is being utilized for the parent project.",
        "type": "string"
      },
      "single_file_mode": {
        "default": false,
        "description": "Single file mode, do not convert other images in the folder. Options: true, false (default).",
        "type": "boolean"
      },
      "tag": {
        "default": "",
        "description": "The tag to be added to one output file upon run completion.",
        "type": "string"
      },
      "tag_on_failure": {
        "default": false,
        "description": "If true, will append the tag 'dcm2niix-failure' on the input file if the gear fails",
        "type": "boolean"
      },
      "text_notes_private": {
        "default": false,
        "description": "Text notes include private patient details. Options: true, false (default).",
        "type": "boolean"
      }
    },
    "custom": {
      "flywheel": {
        "classification": {
          "function": [
            "Conversion"
          ],
          "modality": [
            "Any"
          ],
          "organ": [
            "Any"
          ],
          "species": [
            "Phantom",
            "Human",
            "Animal",
            "Other"
          ],
          "therapeutic_area": [
            "Any"
          ]
        },
        "suite": "Conversion"
      },
      "gear-builder": {
        "category": "converter",
        "image": "flywheel/dcm2niix:2.1.2_1.0.20240202"
      },
      "license": {
        "main": {
          "name": "Other",
          "url": "https://github.com/rordenlab/dcm2niix/blob/v1.0.20230411/license.txt"
        }
      }
    },
    "description": "Implementation of Chris Rorden's dcm2niix tool for converting DICOM (or PAR/REC) to NIfTI (or NRRD).",
    "environment": {
      "BUILD_TIME": "2023-03-01T08:58:30Z",
      "COMMIT_REF": "main",
      "COMMIT_SHA": "0481c74f",
      "DCMCOMMIT": "e2ead4b3c3b6d9763ca17638c10e3b407bf3f21d",
      "EDITOR": "micro",
      "FIXDCMCOMMIT": "918ee3327174c3c736e7b3839a556e0a709730c8",
      "FLYWHEEL": "/flywheel/v0",
      "FSLOUTPUTTYPE": "NIFTI_GZ",
      "GPG_KEY": "A035C8C19219BA821ECEA86B64E628F8D684696D",
      "LANG": "C.UTF-8",
      "PATH": "/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
      "PROMETHEUS_MULTIPROC_DIR": "/var/metrics",
      "PWD": "/flywheel/v0",
      "PYSITE": "/usr/local/lib/python3.11/site-packages",
      "PYTHON_GET_PIP_SHA256": "394be00f13fa1b9aaa47e911bdb59a09c3b2986472130f30aa0bfaf7f3980637",
      "PYTHON_GET_PIP_URL": "https://github.com/pypa/get-pip/raw/d5cb0afaf23b8520f1bbcfed521017b4a95f5c01/public/get-pip.py",
      "PYTHON_PIP_VERSION": "22.3.1",
      "PYTHON_SETUPTOOLS_VERSION": "65.5.1",
      "PYTHON_VERSION": "3.11.2",
      "SETUPTOOLS_USE_DISTUTILS": "stdlib"
    },
    "inputs": {
      "api-key": {
        "base": "api-key",
        "read-only": false
      },
      "dcm2niix_input": {
        "base": "file",
        "description": "Main input file for the Gear. This can be either a DICOM archive ('<dicom>.zip'), a PAR/REC archive ('<parrec>.zip'), or a single PAR file ('image.PAR' or 'image.par').",
        "optional": false,
        "type": {
          "enum": [
            "dicom",
            "parrec"
          ]
        }
      },
      "rec_file_input": {
        "base": "file",
        "description": "If dcm2niix_input is a single PAR file, the corresponding REC file ('image.REC' or 'image.rec') for one PAR/REC file pair as inputs to the Gear.",
        "optional": true,
        "type": {
          "enum": [
            "parrec"
          ]
        }
      }
    },
    "label": "dcm2niix: DICOM to NIfTI conversion",
    "license": "Other",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "dcm2niix",
    "source": "https://github.com/rordenlab/dcm2niix",
    "url": "https://gitlab.com/flywheel-io/scientific-solutions/gears/dcm2niix",
    "version": "2.1.2_1.0.20240202"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "anonymize_bids": {
            "default": true,
            "type": "boolean"
          },
          "bids_sidecar": {
            "default": "y",
            "enum": [
              "y",
              "n",
              "o"
            ],
            "type": "string"
          },
          "coil_combine": {
            "default": false,
            "type": "boolean"
          },
          "comment": {
            "default": "",
            "type": "string"
          },
          "compress_images": {
            "default": "y",
            "enum": [
              "y",
              "i",
              "n",
              "3"
            ],
            "type": "string"
          },
          "compression_level": {
            "default": 6,
            "maximum": 9,
            "minimum": 1,
            "type": "number"
          },
          "convert_only_series": {
            "default": "all",
            "type": "string"
          },
          "crop": {
            "default": false,
            "type": "boolean"
          },
          "dcm2niix_verbose": {
            "default": false,
            "type": "boolean"
          },
          "debug": {
            "default": false,
            "type": "boolean"
          },
          "decompress_dicoms": {
            "default": false,
            "type": "boolean"
          },
          "filename": {
            "default": "%f_%p_%t_%s",
            "type": "string"
          },
          "ignore_derived": {
            "default": false,
            "type": "boolean"
          },
          "ignore_errors": {
            "default": false,
            "type": "boolean"
          },
          "lossless_scaling": {
            "default": "o",
            "enum": [
              "y",
              "n",
              "o"
            ],
            "type": "string"
          },
          "merge2d": {
            "default": 2,
            "enum": [
              0,
              1,
              2
            ],
            "type": "number"
          },
          "output_nrrd": {
            "default": false,
            "type": "boolean"
          },
          "philips_scaling": {
            "default": true,
            "type": "boolean"
          },
          "remove_incomplete_volumes": {
            "default": false,
            "type": "boolean"
          },
          "sanitize_filename": {
            "default": true,
            "type": "boolean"
          },
          "save_sidecar_as_metadata": {
            "default": "",
            "type": "string"
          },
          "single_file_mode": {
            "default": false,
            "type": "boolean"
          },
          "tag": {
            "default": "",
            "type": "string"
          },
          "tag_on_failure": {
            "default": false,
            "type": "boolean"
          },
          "text_notes_private": {
            "default": false,
            "type": "boolean"
          }
        },
        "required": [
          "anonymize_bids",
          "bids_sidecar",
          "coil_combine",
          "comment",
          "compress_images",
          "compression_level",
          "convert_only_series",
          "crop",
          "dcm2niix_verbose",
          "debug",
          "decompress_dicoms",
          "filename",
          "ignore_derived",
          "ignore_errors",
          "lossless_scaling",
          "merge2d",
          "output_nrrd",
          "philips_scaling",
          "remove_incomplete_volumes",
          "sanitize_filename",
          "save_sidecar_as_metadata",
          "single_file_mode",
          "tag",
          "tag_on_failure",
          "text_notes_private"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "api-key": {
            "type": "object"
          },
          "dcm2niix_input": {
            "properties": {
              "type": {
                "enum": [
                  "dicom",
                  "parrec"
                ]
              }
            },
            "type": "object"
          },
          "rec_file_input": {
            "properties": {
              "type": {
                "enum": [
                  "parrec"
                ]
              }
            },
            "type": "object"
          }
        },
        "required": [
          "api-key",
          "dcm2niix_input"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for dcm2niix: DICOM to NIfTI conversion",
    "type": "object"
  }
}
