{
  "exchange": {
    "git-commit": "6c52031a5e759600e8c3cb60b8901900d3daf2df",
    "rootfs-hash": "sha256:ca22b3dfee662341c67876305777352000524e8f5bb09950e6f30550d4a12403",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-fsl-anat:1.1.2_6.0.1"
  },
  "gear": {
    "author": "Analysis Group, FMRIB, Oxford, UK.",
    "cite": "M.W. Woolrich, S. Jbabdi, B. Patenaude, M. Chappell, S. Makni, T. Behrens, C. Beckmann, M. Jenkinson, S.M. Smith. Bayesian analysis of neuroimaging data in FSL. NeuroImage, 45:S173-86, 2009.",
    "command": "/flywheel/v0/run.py",
    "config": {
      "betfparam": {
        "default": 0.1,
        "description": "Specify f parameter for BET (only used if not running non-linear reg and also wanting brain extraction done).",
        "maximum": 1,
        "minimum": 0.01,
        "type": "number"
      },
      "clobber": {
        "default": true,
        "description": "If .anat directory exist (as specified by -o or default from -i) then delete it and make a new one.",
        "type": "boolean"
      },
      "nobias": {
        "default": false,
        "description": "Turn off steps that do bias field correction (via FAST).",
        "type": "boolean"
      },
      "nocleanup": {
        "default": false,
        "description": "Do not remove intermediate files.",
        "type": "boolean"
      },
      "nocrop": {
        "default": false,
        "description": "Turn off step that does automated cropping (robustfov).",
        "type": "boolean"
      },
      "nononlinreg": {
        "default": false,
        "description": "Turn off step that does non-linear registration (FNIRT).",
        "type": "boolean"
      },
      "noreg": {
        "default": false,
        "description": "Turn off steps that do registration to standard (FLIRT and FNIRT).",
        "type": "boolean"
      },
      "noreorient": {
        "default": false,
        "description": "Turn off step that does reorientation 2 standard (fslreorient2std).",
        "type": "boolean"
      },
      "nosearch": {
        "default": false,
        "description": "Specify that linear registration uses the -nosearch option (FLIRT).",
        "type": "boolean"
      },
      "noseg": {
        "default": false,
        "description": "Turn off step that does tissue-type segmentation (FAST).",
        "type": "boolean"
      },
      "nosubcortseg": {
        "default": false,
        "description": "Turn off step that does sub-cortical segmentation (FIRST).",
        "type": "boolean"
      },
      "s": {
        "default": 10,
        "description": "Specify the value for bias field smoothing (the -l option in FAST).",
        "minimum": 2,
        "type": "integer"
      },
      "t": {
        "default": "T1",
        "description": "Specify the type of image (choose one of T1 T2 PD - default is T1).",
        "type": "string"
      },
      "weakbias": {
        "default": false,
        "description": "Used for images with little and/or smooth bias fields. Sets the smoothing parameter (s) to 20 internally.",
        "type": "boolean"
      }
    },
    "custom": {
      "flywheel": {
        "classification": {
          "function": [
            "Image Processing - Structural"
          ],
          "modality": [
            "MR"
          ],
          "organ": [
            "Brain"
          ],
          "species": [
            "Human"
          ],
          "therapeutic_area": [
            "Neurology",
            "Psychiatry/Psychology"
          ]
        },
        "suite": "Image Processing"
      },
      "gear-builder": {
        "category": "analysis",
        "image": "flywheel/fsl-anat:1.1.2_6.0.1"
      }
    },
    "description": "This tool provides a general pipeline for processing anatomical images (e.g. T1-weighted scans). LICENSING NOTE: FSL software are owned by Oxford University Innovation and license is required for any commercial applications. For commercial licence please contact fsl@innovation.ox.ac.uk. For academic use, an academic license is required which is available by registering on the FSL website. Any use of the software requires that the user obtain the appropriate license. See https://fsl.fmrib.ox.ac.uk/fsldownloads_registration for more information. LICENSING NOTE: FSL software are owned by Oxford University Innovation and license is required for any commercial applications. For commercial licence please contact fsl@innovation.ox.ac.uk. For academic use, an academic license is required which is available by registering on the FSL website. Any use of the software requires that the user obtain the appropriate license. See https://fsl.fmrib.ox.ac.uk/fsldownloads_registration for more information.",
    "environment": {
      "DEBIAN_FRONTEND": "noninteractive",
      "FLYWHEEL": "/flywheel/v0",
      "FSLDIR": "/usr/share/fsl/6.0",
      "FSLMULTIFILEQUIT": "TRUE",
      "FSLOUTPUTTYPE": "NIFTI_GZ",
      "FSLTCLSH": "/usr/bin/tclsh",
      "FSLWISH": "/usr/bin/wish",
      "FSL_DIR": "/usr/share/fsl/6.0",
      "GPG_KEY": "0D96DF4D4110E5C43FBFB17F2D347EA6AA65421D",
      "LANG": "C.UTF-8",
      "LD_LIBRARY_PATH": "/usr/share/fsl/6.0/lib",
      "PATH": "/usr/share/fsl/6.0/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
      "POSSUMDIR": "/usr/share/fsl/6.0",
      "PYTHON_PIP_VERSION": "19.1.1",
      "PYTHON_VERSION": "3.7.3",
      "TZ": "Etc/UTC"
    },
    "inputs": {
      "Image": {
        "base": "file",
        "description": "T1-weighted, T2-weighted, or Proton-Density-weighted anatomical NIfTI file",
        "type": {
          "enum": [
            "nifti"
          ]
        }
      }
    },
    "label": "FSL-ANAT - Anatomical Processing Pipeline",
    "license": "Other",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "fsl-anat",
    "source": "https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/fsl_anat",
    "url": "https://github.com/flywheel-apps/fsl-anat",
    "version": "1.1.2_6.0.1"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "betfparam": {
            "default": 0.1,
            "maximum": 1,
            "minimum": 0.01,
            "type": "number"
          },
          "clobber": {
            "default": true,
            "type": "boolean"
          },
          "nobias": {
            "default": false,
            "type": "boolean"
          },
          "nocleanup": {
            "default": false,
            "type": "boolean"
          },
          "nocrop": {
            "default": false,
            "type": "boolean"
          },
          "nononlinreg": {
            "default": false,
            "type": "boolean"
          },
          "noreg": {
            "default": false,
            "type": "boolean"
          },
          "noreorient": {
            "default": false,
            "type": "boolean"
          },
          "nosearch": {
            "default": false,
            "type": "boolean"
          },
          "noseg": {
            "default": false,
            "type": "boolean"
          },
          "nosubcortseg": {
            "default": false,
            "type": "boolean"
          },
          "s": {
            "default": 10,
            "minimum": 2,
            "type": "integer"
          },
          "t": {
            "default": "T1",
            "type": "string"
          },
          "weakbias": {
            "default": false,
            "type": "boolean"
          }
        },
        "required": [
          "betfparam",
          "clobber",
          "nobias",
          "nocleanup",
          "nocrop",
          "nononlinreg",
          "noreg",
          "noreorient",
          "nosearch",
          "noseg",
          "nosubcortseg",
          "s",
          "t",
          "weakbias"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "Image": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          }
        },
        "required": [
          "Image"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for FSL-ANAT - Anatomical Processing Pipeline",
    "type": "object"
  }
}
