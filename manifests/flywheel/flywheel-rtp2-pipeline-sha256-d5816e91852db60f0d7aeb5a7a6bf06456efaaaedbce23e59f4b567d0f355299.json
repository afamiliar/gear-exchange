{
  "exchange": {
    "git-commit": "577741cde2f57017d7a578cf4eade30fdcb3b347",
    "rootfs-hash": "sha256:d5816e91852db60f0d7aeb5a7a6bf06456efaaaedbce23e59f4b567d0f355299",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-rtp2-pipeline:0.1.0_3.0.4"
  },
  "gear": {
    "author": "flywheel",
    "cite": "Lerma-Usabiaga, G., Liu, M., Paz-Alonso, P. M. & Wandell, B. A. Reproducible Tract Profiles 2 (RTP2) suite, from diffusion MRI acquisition to clinical practice and research. Sci. Rep. 13, 1–13 (2023)",
    "command": "python run.py",
    "config": {
      "ET_angleValues": {
        "default": "45, 45, 25, 25, 25,  5,  5",
        "description": "ET: Angle cutoff values to use during ET tracking (comma-separated list). Using one value is the equivalent of not using ET. IMPORTANT: there need to be the same amount of ET_maxlength values than ET_angleValues.",
        "type": "string"
      },
      "ET_maxlength": {
        "default": "100,150,100,150,200,150,200",
        "description": "ET: Maximum fiber length (in mm) used in the whole brain tractrogram tracking. IMPORTANT: it requires the same amount of values as ET_angleValues. Each ET_maxlength value will be matched in position to every ET_angleValues.",
        "type": "string"
      },
      "ET_minlength": {
        "default": 999,
        "description": "ET: Minimum fiber length (in mm). This value will be used for mrTrix regardless of ET. If 999, the default from mrTrix will be used (see mrTrix documentation).",
        "type": "number"
      },
      "ET_numberFibers": {
        "default": 200000,
        "description": "ET: Number of fibers to generate in each tractotram. As many tractograms as specified with ET_maxlength and ET_angleValues number of pairs will be generated and concatenated.",
        "type": "number"
      },
      "ET_track_stepSizeMm": {
        "default": 999,
        "description": "ET: track stepSizeMm.",
        "type": "number"
      },
      "bval_for_fa": {
        "default": 1000,
        "description": "In multishell data, FA is only obtained for one shell, by default the shell with bval closest to 1000. Edit here if another shell is preferred.",
        "type": "number"
      },
      "fiberWeighting": {
        "default": 1,
        "description": "RTP: Set the amount of weighting that will be applied when calculating tract profiles. 0 means that each fiber contributes equally. 1 means that we apply gaussian weighting where each fibers contribution to the measurement at a node is weighted by its gaussian distance from the tract core. Values greater than 1 mean that the core of the tract is weighted more heavily and fibers further from the core are weighted less heavily (faster fall off than a gaussian).  See ComputeTractProperties.",
        "type": "integer"
      },
      "get_vofparc": {
        "default": false,
        "description": "If conditions are met (whole brain tractogram is created and arcuate fasciculus is tracked), obtain VOF and pARC bundles.",
        "type": "boolean"
      },
      "life_discretization": {
        "default": 360,
        "description": "LiFE: Discretization parameter (this parameter defines the resolution of the Phi tensor in describing the orientation of the fascicles in the connectome (number of orientations encoded in Phi, more specifically the size of Phi in mode 1)",
        "type": "number"
      },
      "life_num_iterations": {
        "default": 10,
        "description": "LiFE: Number of iterations for LiFE algorithm.",
        "type": "number"
      },
      "life_runLife": {
        "default": false,
        "description": "LiFE: Run LiFE algorithm.",
        "type": "boolean"
      },
      "life_saveOutput": {
        "default": false,
        "description": "LiFE: Save outputs from LiFE algorithm.",
        "type": "boolean"
      },
      "life_test": {
        "default": false,
        "description": "LiFE: Run LiFE in test mode.",
        "type": "boolean"
      },
      "life_writePDB": {
        "default": false,
        "description": "LiFE: Write fibers out in PDB. This is a legacy format for the program Quench.",
        "type": "boolean"
      },
      "mrtrix_autolmax": {
        "default": true,
        "description": "MRTRIX: Calculate lmax automatically from number of directions",
        "type": "boolean"
      },
      "mrtrix_lmax": {
        "default": 6,
        "description": "MRTRIX: specify lmax for CSD. Only used if mrtrix_autolmax==0.",
        "type": "integer"
      },
      "mrtrix_mrTrixAlgo": {
        "default": "iFOD2",
        "description": "MRTRIX: Tracking algorithm for mrTrix's tckgen. Options are: iFOD2, iFOD2, SD_STREAM, Tensor_Det, Tensor_prob.",
        "type": "string"
      },
      "mrtrix_useACT": {
        "default": false,
        "description": "MRTRIX: Use ACT for tracking",
        "type": "boolean"
      },
      "numberOfNodes": {
        "default": 100,
        "description": "AFQ: The number of nodes to represent each fiber.",
        "type": "integer"
      },
      "save_output": {
        "default": false,
        "description": "If true saves intermediate files. Usually for testing and troubleshooting.",
        "type": "boolean"
      },
      "sift_nFibers": {
        "default": 500000,
        "description": "SIFT: Reduce whole brain tractogram until this number of fibers.",
        "type": "number"
      },
      "sift_runSift": {
        "default": true,
        "description": "SIFT: Run SIFT algorithm.",
        "type": "boolean"
      },
      "track_faFodThresh": {
        "default": 999,
        "description": "MRTRIX: Stopping criteria when tracking, FA for DTI and FOD for CSD. Mrtrix uses defaults depending on the algo and depending on the algo these number will refer to FA or FOD. If 999 use the defaults, otherwise use the value here. See mrTrix docs for defaults",
        "type": "number"
      },
      "track_faMaskThresh": {
        "default": 0.3,
        "description": "MRTRIX: FA mask threshold from which to initialize tracking. It is used to create the wmMask that is used to seed the whole brain connectome in mrTrix",
        "type": "number"
      }
    },
    "custom": {
      "flywheel": {
        "suite": "RTP2 Diffusion Pipeline"
      },
      "gear-builder": {
        "category": "analysis",
        "image": "flywheel/rtp2-pipeline:0.1.0_3.0.4"
      }
    },
    "description": "This gear contains a multi-step pipeline designed to run MRtrix3, LiFE and SIFT, ET, and AFQ. The preprocessing is done in RTP2-preproc. MRTrix3 + Ensemble Tractography + LiFE generate a connectome which is then run through a mrtrix based tracking system. Then tract profiles of tissue properties for major fiber tracts in the brain are generated. ROIs are generated using Freesurferator. IT is possible to create ROI2ROI tracts as well. Required inputs are (1) DWI NIfTI image, (2) BVEC file, (3) BVAL file, (4) Anatomical NIfTI file, (5) fs.zip with the ROIs in the individual space generated by Freesurferator gear, and (6) tractparams.csv, a comma separated file specifying the details of all the fasciculus to be tracked.",
    "environment": {
      "ANTSPATH": "/opt/ants/bin",
      "ARTHOME": "/opt/art",
      "FLYWHEEL": "/flywheel/v0",
      "FREESURFER_HOME": "/opt/freesurfer",
      "FSLDIR": "/opt/fsl",
      "FSLMULTIFILEQUIT": "TRUE",
      "FSLOUTPUTTYPE": "NIFTI_GZ",
      "FSLTCLSH": "/opt/fsl/bin/fsltclsh",
      "FSLWISH": "/opt/fsl/bin/fslwish",
      "GPG_KEY": "E3FF2839C048B25C084DEBE9B26995E310250568",
      "LANG": "C.UTF-8",
      "LD_LIBRARY_PATH": "/opt/mcr/v99/runtime/glnxa64:/opt/mcr/v99/bin/glnxa64:/opt/mcr/v99/sys/os/glnxa64:/opt/mcr/v99/extern/bin/glnxa64:/opt/fsl/lib:",
      "MCR_CACHE_FOLDER_NAME": "/flywheel/v0/output/.mcrCache9.9",
      "MCR_CACHE_ROOT": "/flywheel/v0/output",
      "MRTRIX_TMPFILE_DIR": "/flywheel/v0/output/tmp",
      "PATH": "/opt/mrtrix3/bin:/opt/ants/bin:/opt/art/bin:/opt/fsl/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
      "PWD": "/flywheel/v0",
      "PYTHON_GET_PIP_SHA256": "1e501cf004eac1b7eb1f97266d28f995ae835d30250bec7f8850562703067dc6",
      "PYTHON_GET_PIP_URL": "https://github.com/pypa/get-pip/raw/66030fa03382b4914d4c4d0896961a0bdeeeb274/public/get-pip.py",
      "PYTHON_PIP_VERSION": "22.0.4",
      "PYTHON_SETUPTOOLS_VERSION": "58.1.0",
      "PYTHON_VERSION": "3.9.15",
      "TEMPLATES": "/templates",
      "XAPPLRESDIR": "/opt/mcr/v99/X11/app-defaults"
    },
    "flywheel": "0",
    "inputs": {
      "anatomical": {
        "base": "file",
        "description": "Anatomical (e.g., t1-weighted) NIfTI file. The pipeline asumes that the diffusion data is in the same space as the anatomical and that the aligmnent was performed in the preprocessing step.",
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "bval": {
        "base": "file",
        "description": "BVAL file",
        "type": {
          "enum": [
            "bval"
          ]
        }
      },
      "bvec": {
        "base": "file",
        "description": "BVEC file.",
        "type": {
          "enum": [
            "bvec"
          ]
        }
      },
      "dwi": {
        "base": "file",
        "description": "DWI NIfTI file",
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "fs": {
        "base": "file",
        "description": "zip file generated in the Freesurfer gear with some base files (brainmask and aparc+aseg) and a folder with all the ROIs generated in the FS gear. It will contain the subcortical and cortical parcellations from freesurfer, as well as hippocampal and thalamic segmentations, a cerebellum atlas and visual cortex segmentation provided by Neuropythy. Please check FS gear for more information. This .zip file can be manually generated and uploaded as an acquisition.  zip file format: in the base it will contain only a folder called fs/. Inside this folder, there will be some files (brainmask.nii.gz, aparc+aseg.nii.gz...) and a folder called ROIs. Therefore, inside fs/ROIs/ any roi can be found. The system will unzip the fs.zip internally and at the beginning of the pipeline it will check if the ROIs are there. The ROis can be dilated or concatenated, see the documentation.",
        "type": {
          "enum": [
            "zip"
          ]
        }
      },
      "fsmask": {
        "base": "file",
        "description": "Anatomical brainmask, usually obtained from freesurfer, but it can be added manually. The pipeline asumes that the diffusion data is in the same space, and that the aligmnent was performed in the previous rtp2-preproc step.",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "qmap": {
        "base": "file",
        "description": "Quantitative map registered to the anatomical image, that will be used to obtain tract and ROI metrics. If this file is passed, three new csv files will show up in the output: ROI_qmap.csv, RTP_qmap.csv and RTP_C2ROIqmap.csv. Even though this option was created for qMRI maps, it really can be used with any scalar map with a value per voxel (for example NODDI maps). It needs to be in the same space as the anatomical, this registrations is usually done with the ANTs gear or in rtp2-preproc when implemented.",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "tractparams": {
        "base": "file",
        "description": "Only required if tracking is required. It is a csv file with all the options to create the tracts, one line per each tract. One example csv file can be obtained from here: https://osf.io/download/m2p8e/. This tractparams requires a fs.zip file generated in Freesurferator with the example_MNI_ROIs, containing the CC ROIs. Any error in this csv file will make the pipeline fail at some point. The ROI names specified in this file will be expected to find in the fs.zip inside the fs folder. IMPORTANT: the code is preparared so that the fs.zip will be read from the output of the Freesurferator gear. But the ROIs can be manually generated (or automatically based on fMRI individual results, for example). zip file format: in the base it will contain only a folder called fs/. Inside this folder, there will be some files (brainmask.nii.gz, aparc+aseg.nii.gz...) and a folder called ROIs. Therefore, inside fs/ROIs/ any roi can be found. The system will unzip the fs.zip internally and at the beginning of the pipeline it will check if the ROIs are there. The ROis can be dilated or concatenated, see the documentation.",
        "optional": true,
        "type": {
          "enum": [
            "csv"
          ]
        }
      }
    },
    "label": "RTP2 Pipeline: Diffusion MRI analysis, modeling, tracking and metrics",
    "license": "Other",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "rtp2-pipeline",
    "source": "https://gitlab.com/flywheel-io/scientific-solutions/gears/rtp2-pipeline",
    "url": "https://gitlab.com/flywheel-io/scientific-solutions/gears/rtp2-pipeline",
    "version": "0.1.0_3.0.4"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "ET_angleValues": {
            "default": "45, 45, 25, 25, 25,  5,  5",
            "type": "string"
          },
          "ET_maxlength": {
            "default": "100,150,100,150,200,150,200",
            "type": "string"
          },
          "ET_minlength": {
            "default": 999,
            "type": "number"
          },
          "ET_numberFibers": {
            "default": 200000,
            "type": "number"
          },
          "ET_track_stepSizeMm": {
            "default": 999,
            "type": "number"
          },
          "bval_for_fa": {
            "default": 1000,
            "type": "number"
          },
          "fiberWeighting": {
            "default": 1,
            "type": "integer"
          },
          "get_vofparc": {
            "default": false,
            "type": "boolean"
          },
          "life_discretization": {
            "default": 360,
            "type": "number"
          },
          "life_num_iterations": {
            "default": 10,
            "type": "number"
          },
          "life_runLife": {
            "default": false,
            "type": "boolean"
          },
          "life_saveOutput": {
            "default": false,
            "type": "boolean"
          },
          "life_test": {
            "default": false,
            "type": "boolean"
          },
          "life_writePDB": {
            "default": false,
            "type": "boolean"
          },
          "mrtrix_autolmax": {
            "default": true,
            "type": "boolean"
          },
          "mrtrix_lmax": {
            "default": 6,
            "type": "integer"
          },
          "mrtrix_mrTrixAlgo": {
            "default": "iFOD2",
            "type": "string"
          },
          "mrtrix_useACT": {
            "default": false,
            "type": "boolean"
          },
          "numberOfNodes": {
            "default": 100,
            "type": "integer"
          },
          "save_output": {
            "default": false,
            "type": "boolean"
          },
          "sift_nFibers": {
            "default": 500000,
            "type": "number"
          },
          "sift_runSift": {
            "default": true,
            "type": "boolean"
          },
          "track_faFodThresh": {
            "default": 999,
            "type": "number"
          },
          "track_faMaskThresh": {
            "default": 0.3,
            "type": "number"
          }
        },
        "required": [
          "ET_angleValues",
          "ET_maxlength",
          "ET_minlength",
          "ET_numberFibers",
          "ET_track_stepSizeMm",
          "bval_for_fa",
          "fiberWeighting",
          "get_vofparc",
          "life_discretization",
          "life_num_iterations",
          "life_runLife",
          "life_saveOutput",
          "life_test",
          "life_writePDB",
          "mrtrix_autolmax",
          "mrtrix_lmax",
          "mrtrix_mrTrixAlgo",
          "mrtrix_useACT",
          "numberOfNodes",
          "save_output",
          "sift_nFibers",
          "sift_runSift",
          "track_faFodThresh",
          "track_faMaskThresh"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "anatomical": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "bval": {
            "properties": {
              "type": {
                "enum": [
                  "bval"
                ]
              }
            },
            "type": "object"
          },
          "bvec": {
            "properties": {
              "type": {
                "enum": [
                  "bvec"
                ]
              }
            },
            "type": "object"
          },
          "dwi": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "fs": {
            "properties": {
              "type": {
                "enum": [
                  "zip"
                ]
              }
            },
            "type": "object"
          },
          "fsmask": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "qmap": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "tractparams": {
            "properties": {
              "type": {
                "enum": [
                  "csv"
                ]
              }
            },
            "type": "object"
          }
        },
        "required": [
          "anatomical",
          "bval",
          "bvec",
          "dwi",
          "fs"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for RTP2 Pipeline: Diffusion MRI analysis, modeling, tracking and metrics",
    "type": "object"
  }
}
