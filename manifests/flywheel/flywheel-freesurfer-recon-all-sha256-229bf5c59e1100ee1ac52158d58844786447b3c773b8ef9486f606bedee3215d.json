{
  "exchange": {
    "git-commit": "f4a307964a0431663e12a0d78439ca2b36550f0b",
    "rootfs-hash": "sha256:229bf5c59e1100ee1ac52158d58844786447b3c773b8ef9486f606bedee3215d",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-freesurfer-recon-all:2.0.2_7.2.0"
  },
  "gear": {
    "author": "Laboratory for Computational Neuroimaging <freesurfer@nmr.mgh.harvard.edu>",
    "cite": "For citation information, please visit: https://surfer.nmr.mgh.harvard.edu/fswiki/FreeSurferMethodsCitation.",
    "command": "/root/miniconda3/bin/python3 run.py",
    "config": {
      "debug": {
        "default": false,
        "description": "Log debug messages",
        "type": "boolean"
      },
      "freesurfer_license_key": {
        "description": "Text from license file generated during FreeSurfer registration. *Entries should be space separated*",
        "optional": true,
        "type": "string"
      },
      "gear-brainstem_structures": {
        "default": true,
        "description": "Generate automated segmentation of four different brainstem structures from the input T1 scan: medulla oblongata, pons, midbrain and superior cerebellar peduncle (SCP). We use a Bayesian segmentation algorithm that relies on a probabilistic atlas of the brainstem (and neighboring brain structures) built upon manual delineations of the structures on interest in 49 scans (10 for the brainstem structures, 39 for the surrounding structures). The delineation protocol for the brainstem was designed by Dr. Adam Boxer and his team at the UCSF Memory and Aging Center, and is described in the paper. Choosing this option will write <subject_id>_BrainstemStructures.csv to the final results. See: https://surfer.nmr.mgh.harvard.edu/fswiki/BrainstemSubstructures for more info. (Default=true)",
        "type": "boolean"
      },
      "gear-convert_stats": {
        "default": true,
        "description": "Convert FreeSurfer stats files to CSV. (Default=true). Converts a subcortical stats file created by recon-all and/or mri_segstats (eg, aseg.stats) into a table in which each line is a subject and each column is a segmentation. The values are the volume of the segmentation in mm3 or the mean intensity over the structure. Also Converts a cortical stats file created by recon-all and or mris_anatomical_stats (eg, ?h.aparc.stats) into a table in which each line is a subject and each column is a parcellation. By default, the values are the area of the parcellation in mm2.",
        "type": "boolean"
      },
      "gear-convert_surfaces": {
        "default": true,
        "description": "Convert selected FreeSurfer surface files to OBJ format. (Default=true)",
        "type": "boolean"
      },
      "gear-convert_volumes": {
        "default": true,
        "description": "Convert selected FreeSurfer volume files (mgz) to NIfTI format. (Default=true)",
        "type": "boolean"
      },
      "gear-dry-run": {
        "default": false,
        "description": "Do everything except actually executing recon-all",
        "type": "boolean"
      },
      "gear-gtmseg": {
        "default": false,
        "description": "After running recon-all run gtmseg on the subject. (Default=False).",
        "type": "boolean"
      },
      "gear-hippocampal_subfields": {
        "default": true,
        "description": "Generates an automated segmentation of the hippocampal subfields based on a statistical atlas built primarily upon ultra-high resolution (~0.1 mm isotropic) ex vivo MRI data. Choosing this option will write <subject_id>_HippocampalSubfields.csv to the final results. See: https://surfer.nmr.mgh.harvard.edu/fswiki/HippocampalSubfields for more info. (Default=true)",
        "type": "boolean"
      },
      "gear-hypothalamic_subunits": {
        "default": false,
        "description": "After running recon-all run Segmentation of hypothalamic subunits (mri_segment_hypothalamic_subunits) on the subject. (Default=False).",
        "type": "boolean"
      },
      "gear-log-level": {
        "default": "INFO",
        "description": "Gear Log verbosity level (INFO|DEBUG)",
        "enum": [
          "INFO",
          "DEBUG"
        ],
        "type": "string"
      },
      "gear-postprocessing-only": {
        "default": false,
        "description": "If a previous recon-all .zip file is given as input, do not re-run recon-all, instead only run the additional post-processing commands",
        "type": "boolean"
      },
      "gear-register_surfaces": {
        "default": true,
        "description": "Runs the xhemireg and surfreg scripts on your subject after having run recon-all in order to register the subject's left and inverted-right hemispheres to the fsaverage_sym subject. (The fsaverage_sym subject is a version of the fsaverage subject with a single the left-right symmetric pseudo-hemisphere.) (Default=true)",
        "type": "boolean"
      },
      "gear-thalamic_nuclei": {
        "default": false,
        "description": "Produce a parcellation of the thalamus into 25 different nuclei, using a probabilistic atlas built with histological data. Choosing this option will produce 3 files in the subject's mri directory: ThalamicNuclei.v12.T1.volumes.txt, ThalamicNuclei.v12.T1.mgz, and ThalamicNuclei.v12.T1.FSvoxelSpace.mgz, and 2 files in the stats directory: thalamic-nuclei.lh.v12.T1.stats and thalamic-nuclei.rh.v12.T1.stats. See: https://surfer.nmr.mgh.harvard.edu/fswiki/ThalamicNuclei for more info. (Default=false)",
        "type": "boolean"
      },
      "n_cpus": {
        "description": "Command line option to set the number of processors to use in recon-all execution. (optional, defaults to 'core_count' within host). Overridden by including the flag -openmp <num> after -parallel, where <num> is the number of processors you'd like to use. Minimum of 1. If set greater than the cores available, that maximum will be used. If 'parallel' is False, this is disregarded.",
        "minimum": 0,
        "optional": true,
        "type": "integer"
      },
      "parallel": {
        "default": false,
        "description": "Command line option to run recon-all in parallel. (Default=False). Setting to True will instruct the binaries to use 4 processors (cores), meaning, 4 threads will run in parallel in some operations. Adjust n_cpus for more (or less) than 4 cores.",
        "type": "boolean"
      },
      "reconall_options": {
        "default": "-all -qcache",
        "description": "Command line options to the recon-all algorithm. (Default='-all -qcache'). By default we enable '-all' and '-qcache'. '-all' runs the entire pipeline and '-qcache' will resample data onto the average subject (called fsaverage) and smooth it at various FWHM (full-width/half-max) values, usually 0, 5, 10, 15, 20, and 25mm, which can speed later processing. Note that modification of these options may result in failure if the options are not recognized.",
        "type": "string"
      },
      "subject_id": {
        "description": "Desired subject ID. Any spaces in the subject_id will be replaced with underscores and will be used to name the resulting FreeSurfer output directory. NOTE: If using a previous Gear output as input the subject code will be parsed from the input archive, however it should still be provided here for good measure.",
        "optional": true,
        "type": "string"
      }
    },
    "custom": {
      "flywheel": {
        "classification": {
          "function": [
            "Image Processing - Structural"
          ],
          "modality": [
            "MR"
          ],
          "organ": [
            "Brain"
          ],
          "species": [
            "Human"
          ],
          "therapeutic_area": [
            "Psychiatry/Psychology",
            "Neurology"
          ]
        },
        "show-job": true,
        "suite": "Image Processing"
      },
      "gear-builder": {
        "category": "analysis",
        "image": "flywheel/freesurfer-recon-all:2.0.2_7.2.0"
      }
    },
    "description": "FreeSurfer version 7.2.0 Release (July 19, 2021). This gear takes an anatomical NIfTI file and performs all of the FreeSurfer cortical reconstruction process. Outputs are provided in a zip file and include the entire output directory tree from Recon-All. Configuration options exist for setting the subject ID and for converting outputs to NIfTI, OBJ, and CSV. FreeSurfer is a software package for the analysis and visualization of structural and functional neuroimaging data from cross-sectional or longitudinal studies. It is developed by the Laboratory for Computational Neuroimaging at the Athinoula A. Martinos Center for Biomedical Imaging. Please see https://surfer.nmr.mgh.harvard.edu/fswiki/FreeSurferSoftwareLicense for license information.",
    "environment": {
      "CPATH": "/root/miniconda3/include/:",
      "FLYWHEEL": "/flywheel/v0",
      "FMRI_ANALYSIS_DIR": "/usr/local/freesurfer/fsfast",
      "FREESURFER": "/usr/local/freesurfer",
      "FREESURFER_HOME": "/usr/local/freesurfer",
      "FSFAST_HOME": "/usr/local/freesurfer/fsfast",
      "FSF_OUTPUT_FORMAT": "nii.gz",
      "FS_OVERRIDE": "0",
      "FUNCTIONALS_DIR": "/usr/local/freesurfer/sessions",
      "LANG": "C.UTF-8",
      "LOCAL_DIR": "/usr/local/freesurfer/local",
      "MINC_BIN_DIR": "/usr/local/freesurfer/mni/bin",
      "MINC_LIB_DIR": "/usr/local/freesurfer/mni/lib",
      "MNI_DATAPATH": "/usr/local/freesurfer/mni/data",
      "MNI_DIR": "/usr/local/freesurfer/mni",
      "MNI_PERL5LIB": "/usr/local/freesurfer/mni/share/perl5",
      "OS": "Linux",
      "PATH": "/root/miniconda3/bin:/usr/local/freesurfer/bin:/usr/local/freesurfer/fsfast/bin:/usr/local/freesurfer/tktools:/usr/local/freesurfer/mni/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
      "PERL5LIB": "/usr/local/freesurfer/mni/share/perl5",
      "PWD": "/flywheel/v0",
      "PYTHONNOUSERSITE": "1",
      "PYTHONUNBUFFERED": "1",
      "SHLVL": "1",
      "SUBJECTS_DIR": "/usr/local/freesurfer/subjects",
      "_": "/usr/bin/printenv"
    },
    "inputs": {
      "anatomical": {
        "base": "file",
        "description": "Anatomical NIfTI file, DICOM archive, or previous freesurfer-recon-all zip archive. NOTE: A freesurfer-recon-all Gear output can be used provided the filename is preserved from its initial output (e.g., freesurfer-recon-all_<subject_code>*.zip)",
        "type": {
          "enum": [
            "nifti",
            "dicom",
            "archive"
          ]
        }
      },
      "api-key": {
        "base": "api-key",
        "read-only": false
      },
      "expert": {
        "base": "file",
        "description": "A user-created file containing special options to include in the command string. The file should contain as the first item the name of the command, and the items following it on rest of the line will be passed as the extra options.  See Freesurfer documentation https://surfer.nmr.mgh.harvard.edu/fswiki/recon-all#ExpertOptionsFile for more information and examples.",
        "optional": true
      },
      "freesurfer_license_file": {
        "base": "file",
        "description": "FreeSurfer license file, provided during registration with FreeSurfer. This file will by copied to the $FSHOME directory and used during execution of the Gear.",
        "optional": true
      },
      "t1w_anatomical_2": {
        "base": "file",
        "description": "Additional anatomical NIfTI file",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "t1w_anatomical_3": {
        "base": "file",
        "description": "Additional anatomical NIfTI file",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "t1w_anatomical_4": {
        "base": "file",
        "description": "Additional anatomical NIfTI file",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "t1w_anatomical_5": {
        "base": "file",
        "description": "Additional anatomical NIfTI file",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "t2w_anatomical": {
        "base": "file",
        "description": "T2w or FLAIR Anatomical NIfTI or DICOM file",
        "optional": true,
        "type": {
          "enum": [
            "nifti",
            "dicom"
          ]
        }
      }
    },
    "label": "Freesurfer Recon-All. Run recon-all on one or more T1w images.",
    "license": "BSD-3-Clause",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "freesurfer-recon-all",
    "source": "https://surfer.nmr.mgh.harvard.edu",
    "url": "https://gitlab.com/flywheel-io/scientific-solutions/gears/freesurfer-recon-all",
    "version": "2.0.2_7.2.0"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "debug": {
            "default": false,
            "type": "boolean"
          },
          "freesurfer_license_key": {
            "type": "string"
          },
          "gear-brainstem_structures": {
            "default": true,
            "type": "boolean"
          },
          "gear-convert_stats": {
            "default": true,
            "type": "boolean"
          },
          "gear-convert_surfaces": {
            "default": true,
            "type": "boolean"
          },
          "gear-convert_volumes": {
            "default": true,
            "type": "boolean"
          },
          "gear-dry-run": {
            "default": false,
            "type": "boolean"
          },
          "gear-gtmseg": {
            "default": false,
            "type": "boolean"
          },
          "gear-hippocampal_subfields": {
            "default": true,
            "type": "boolean"
          },
          "gear-hypothalamic_subunits": {
            "default": false,
            "type": "boolean"
          },
          "gear-log-level": {
            "default": "INFO",
            "enum": [
              "INFO",
              "DEBUG"
            ],
            "type": "string"
          },
          "gear-postprocessing-only": {
            "default": false,
            "type": "boolean"
          },
          "gear-register_surfaces": {
            "default": true,
            "type": "boolean"
          },
          "gear-thalamic_nuclei": {
            "default": false,
            "type": "boolean"
          },
          "n_cpus": {
            "minimum": 0,
            "type": "integer"
          },
          "parallel": {
            "default": false,
            "type": "boolean"
          },
          "reconall_options": {
            "default": "-all -qcache",
            "type": "string"
          },
          "subject_id": {
            "type": "string"
          }
        },
        "required": [
          "debug",
          "gear-brainstem_structures",
          "gear-convert_stats",
          "gear-convert_surfaces",
          "gear-convert_volumes",
          "gear-dry-run",
          "gear-gtmseg",
          "gear-hippocampal_subfields",
          "gear-hypothalamic_subunits",
          "gear-log-level",
          "gear-postprocessing-only",
          "gear-register_surfaces",
          "gear-thalamic_nuclei",
          "parallel",
          "reconall_options"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "anatomical": {
            "properties": {
              "type": {
                "enum": [
                  "nifti",
                  "dicom",
                  "archive"
                ]
              }
            },
            "type": "object"
          },
          "api-key": {
            "type": "object"
          },
          "expert": {
            "properties": {},
            "type": "object"
          },
          "freesurfer_license_file": {
            "properties": {},
            "type": "object"
          },
          "t1w_anatomical_2": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "t1w_anatomical_3": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "t1w_anatomical_4": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "t1w_anatomical_5": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "t2w_anatomical": {
            "properties": {
              "type": {
                "enum": [
                  "nifti",
                  "dicom"
                ]
              }
            },
            "type": "object"
          }
        },
        "required": [
          "anatomical",
          "api-key"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for Freesurfer Recon-All. Run recon-all on one or more T1w images.",
    "type": "object"
  }
}
