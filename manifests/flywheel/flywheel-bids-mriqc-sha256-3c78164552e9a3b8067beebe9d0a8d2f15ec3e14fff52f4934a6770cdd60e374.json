{
  "exchange": {
    "git-commit": "9d4c01ce787db85dd152815b2a46433a31abbe9e",
    "rootfs-hash": "sha256:3c78164552e9a3b8067beebe9d0a8d2f15ec3e14fff52f4934a6770cdd60e374",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-bids-mriqc:2.1.2_24.0.0"
  },
  "gear": {
    "author": "Poldrack Lab, Stanford University",
    "cite": "Esteban O, Birman D, Schaer M, Koyejo OO, Poldrack RA, Gorgolewski KJ; MRIQC: Advancing the Automatic Prediction of Image Quality in MRI from Unseen Sites; PLOS ONE 12(9):e0184661; doi:10.1371/journal.pone.0184661.",
    "command": "/opt/flypy/bin/python run.py",
    "config": {
      "bids_app_command": {
        "default": "",
        "description": "OPTIONAL. The gear will run the algorithm defaults, if no command is supplied. For usage, see https://mriqc.readthedocs.io/en/latest/running.html#command-line-interface. Example command: mriqc bids_dir output_dir participant [arg1 [arg2 ...]]",
        "type": "string"
      },
      "debug": {
        "default": false,
        "description": "Log debug messages",
        "type": "boolean"
      },
      "gear-FREESURFER_LICENSE": {
        "default": "",
        "description": "Text from license file generated during FreeSurfer registration. *Entries should be space separated*",
        "type": "string"
      },
      "gear-dry-run": {
        "default": false,
        "description": "Do everything Flywheel-related except actually execute BIDS App command. Different from passing '--dry-run' in the BIDS App command.",
        "type": "boolean"
      },
      "gear-expose-all-outputs": {
        "default": false,
        "description": "Keep ALL the extra output files that are created during the run in addition to the normal, zipped output. Note: This option may cause a gear failure because there are too many files for the engine.",
        "type": "boolean"
      },
      "gear-intermediate-files": {
        "default": "",
        "description": "Space separated list of FILES to retain from the intermediate work directory.",
        "type": "string"
      },
      "gear-intermediate-folders": {
        "default": "",
        "description": "Space separated list of FOLDERS to retain from the intermediate work directory.",
        "type": "string"
      },
      "gear-post-processing-only": {
        "default": false,
        "description": "REQUIRES archive file. Gear will skip the BIDS algorithm and go straight to generating the HTML reports and processing metadata.",
        "type": "boolean"
      },
      "gear-save-intermediate-output": {
        "default": false,
        "description": "Gear will save ALL intermediate output into {{bids_app_binary}}_work.zip",
        "type": "boolean"
      },
      "no-sub": {
        "default": false,
        "description": "Turn off submission of anonymized quality metrics to MRIQC's metrics repository",
        "type": "boolean"
      }
    },
    "custom": {
      "analysis-level": "participant",
      "bids-app-binary": "mriqc",
      "bids-app-data-types": [
        "anat",
        "func",
        "dwi",
        "perf"
      ],
      "flywheel": {
        "classification": {
          "function": [
            "Quality Assurance"
          ],
          "modality": [
            "MR"
          ],
          "organ": [
            "Brain"
          ],
          "species": [
            "Human"
          ],
          "therapeutic_area": [
            "Psychiatry/Psychology",
            "Neurology"
          ]
        },
        "show-job": true,
        "suite": "Quality Assurance"
      },
      "gear-builder": {
        "category": "analysis",
        "image": "flywheel/bids-mriqc:2.1.2_24.0.0"
      },
      "license": {
        "dependencies": [
          {
            "name": "Other",
            "url": "https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Licence"
          },
          {
            "name": "Other",
            "url": "https://github.com/ANTsX/ANTs/blob/v2.3.4/COPYING.txt"
          },
          {
            "name": "Other",
            "url": "https://surfer.nmr.mgh.harvard.edu/fswiki/License"
          }
        ],
        "main": {
          "name": "MIT"
        },
        "non-commercial-use-only": true
      }
    },
    "description": "MRIQC (23.1.0) extracts no-reference image quality metrics (IQMs) from T1w and T2w structural and functional magnetic resonance imaging data.  For more commandline options, please visit https://mriqc.readthedocs.io/en/latest/running.html. LICENSING NOTE: FSL software are owned by Oxford University Innovation and license is required for any commercial applications. For commercial licence please contact fsl@innovation.ox.ac.uk. For academic use, an academic license is required which is available by registering on the FSL website. Any use of the software requires that the user obtain the appropriate license. See https://fsl.fmrib.ox.ac.uk/fsl/docs/#/license for more information.",
    "environment": {
      "AFNI_DIR": "/opt/afni",
      "AFNI_IMSAVE_WARNINGS": "NO",
      "AFNI_MODELPATH": "/opt/afni/models",
      "AFNI_PLUGINPATH": "/opt/afni/plugins",
      "AFNI_TTATLAS_DATASET": "/opt/afni/atlases",
      "ANTSPATH": "/opt/ants",
      "CONDA_PATH": "/opt/conda",
      "CPATH": "/opt/conda/include:",
      "FLYWHEEL": "/flywheel/v0",
      "FREESURFER_HOME": "/opt/freesurfer",
      "HOME": "/home/mriqc",
      "IS_DOCKER_8395080871": "1",
      "LANG": "en_US.UTF-8",
      "LC_ALL": "en_US.UTF-8",
      "LD_LIBRARY_PATH": "/usr/lib/x86_64-linux-gnu:/opt/conda/lib",
      "MKL_NUM_THREADS": "1",
      "OMP_NUM_THREADS": "1",
      "PATH": "/opt/ants:/opt/afni:/opt/conda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
      "PWD": "/flywheel/v0",
      "PYTHONNOUSERSITE": "1",
      "PYTHONUNBUFFERED": "1"
    },
    "inputs": {
      "api-key": {
        "base": "api-key",
        "read-only": false
      },
      "archived_runs": {
        "base": "file",
        "description": "Zip file with data or analyses from previous runs (e.g., FreeSurfer archive)",
        "optional": true
      }
    },
    "label": "BIDS MRIQC: Automatic prediction of quality and visual reporting of MRI scans in BIDS format",
    "license": "Other",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "bids-mriqc",
    "source": "https://github.com/nipreps/mriqc/tree/master",
    "url": "https://gitlab.com/flywheel-io/scientific-solutions/gears/bids-apps/bids-mriqc",
    "version": "2.1.2_24.0.0"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "bids_app_command": {
            "default": "",
            "type": "string"
          },
          "debug": {
            "default": false,
            "type": "boolean"
          },
          "gear-FREESURFER_LICENSE": {
            "default": "",
            "type": "string"
          },
          "gear-dry-run": {
            "default": false,
            "type": "boolean"
          },
          "gear-expose-all-outputs": {
            "default": false,
            "type": "boolean"
          },
          "gear-intermediate-files": {
            "default": "",
            "type": "string"
          },
          "gear-intermediate-folders": {
            "default": "",
            "type": "string"
          },
          "gear-post-processing-only": {
            "default": false,
            "type": "boolean"
          },
          "gear-save-intermediate-output": {
            "default": false,
            "type": "boolean"
          },
          "no-sub": {
            "default": false,
            "type": "boolean"
          }
        },
        "required": [
          "bids_app_command",
          "debug",
          "gear-FREESURFER_LICENSE",
          "gear-dry-run",
          "gear-expose-all-outputs",
          "gear-intermediate-files",
          "gear-intermediate-folders",
          "gear-post-processing-only",
          "gear-save-intermediate-output",
          "no-sub"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "api-key": {
            "type": "object"
          },
          "archived_runs": {
            "properties": {},
            "type": "object"
          }
        },
        "required": [
          "api-key"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for BIDS MRIQC: Automatic prediction of quality and visual reporting of MRI scans in BIDS format",
    "type": "object"
  }
}
