{
  "exchange": {
    "git-commit": "6c52031a5e759600e8c3cb60b8901900d3daf2df",
    "rootfs-hash": "sha256:6dee6281163b3bb3375e4c28c3e896741fef7d42e6d68394745c751b19a8c68b",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-ants-dbm-longitudinal:0.1.1_2.3.5"
  },
  "gear": {
    "author": "Flywheel",
    "cite": "The ANTs Longitudinal Cortical Thickness Pipeline:Tustison, N. J., Holbrook, A. J., Avants, B. B., Roberts, J. M., Cook, P. A., Reagh, Z. M., Duda, J. T., Stone, J. R., Gillen, D. L., & Yassa, M. A., 2017. https://www.biorxiv.org/content/10.1101/170209v1",
    "command": "poetry run python run.py",
    "config": {
      "atlases_template": {
        "default": "OASIS-30_Atropos_template",
        "description": "If predefined atlases are not provided as an input, the gear will use this as the default predefined atlas template.",
        "enum": [
          "OASIS-30_Atropos_template"
        ],
        "type": "string"
      },
      "atropos_iteration": {
        "default": 5,
        "description": "Number of iterations within Atropos.",
        "type": "integer"
      },
      "debug": {
        "default": false,
        "description": "Log debug messages",
        "type": "boolean"
      },
      "denoise_anatomical_image": {
        "default": 1,
        "description": "Denoise anatomical images.",
        "type": "integer"
      },
      "image_dimension": {
        "default": 3,
        "description": "2 or 3 (for 2- or 3-dimensional image)",
        "type": "integer"
      },
      "input_regex": {
        "default": ".*nii\\.gz",
        "description": "Regular expression that matches files to be used as anatomical image inputs. (Default '.*nii\\\\.gz'). https://en.wikipedia.org/wiki/Regular_expression.",
        "type": "string"
      },
      "input_tags": {
        "default": "",
        "description": "Tag(s) that matches files to be used as anatomical image inputs. When multiple tags are specified, they must be comma separated (e.g. anatomical,ANTsLongitudinal)",
        "type": "string"
      },
      "number_of_modalities": {
        "default": 1,
        "description": "Number of modalities used to construct the template:  For example,if one wanted to use multiple modalities consisting of T1, T2, and FA components, it will be 3 modalities.",
        "type": "integer"
      },
      "rigid_alignment_to_SST": {
        "default": 0,
        "description": "If 1, register anatomical images to the single-subject template before processing with antsCorticalThickness. This potentially reduces bias caused by variable orientation and voxel spacing ",
        "type": "integer"
      },
      "rigid_template_update_component": {
        "default": 0,
        "description": "Update the single-subject template with the full affine transform (default 0).If 1, the rigid component of the affine transform will be used to update the template. Using the rigid component is desireable to reduce bias, but variations in the origin or head position across time points can cause the template head to drift out of the field of view.",
        "type": "integer"
      }
    },
    "custom": {
      "flywheel": {
        "classification": {
          "function": [
            "Image Processing - Structural"
          ],
          "modality": [
            "MR"
          ],
          "organ": [
            "Brain"
          ],
          "species": [
            "Human"
          ],
          "therapeutic_area": [
            "Neurology"
          ]
        },
        "suite": "Image Processing"
      },
      "gear-builder": {
        "category": "analysis",
        "image": "flywheel/ants-dbm-longitudinal:0.1.1_2.3.5"
      }
    },
    "description": "ANTs based gear that run antsLongitudinalCorticalThickness.sh shell script and generate two zip archives that contains Single Subject Template and Time Point based template.",
    "environment": {
      "ANTSPATH": "/opt/ants/bin/",
      "ANTS_RANDOM_SEED": "42",
      "FLYWHEEL": "/flywheel/v0",
      "ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS": "42",
      "PATH": "/opt/ants/bin/:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
    },
    "inputs": {
      "api-key": {
        "base": "api-key",
        "read-only": false
      },
      "registered_predefined_atlases": {
        "base": "file",
        "description": "Registered atlases to a population based template",
        "optional": true
      }
    },
    "label": "ANTs DBM Longitudinal",
    "license": "MIT",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "ants-dbm-longitudinal",
    "source": "https://gitlab.com/flywheel-io/flywheel-apps/ants-dbm-longitudinal",
    "url": "https://gitlab.com/flywheel-io/flywheel-apps/ants-dbm-longitudinal",
    "version": "0.1.1_2.3.5"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "atlases_template": {
            "default": "OASIS-30_Atropos_template",
            "enum": [
              "OASIS-30_Atropos_template"
            ],
            "type": "string"
          },
          "atropos_iteration": {
            "default": 5,
            "type": "integer"
          },
          "debug": {
            "default": false,
            "type": "boolean"
          },
          "denoise_anatomical_image": {
            "default": 1,
            "type": "integer"
          },
          "image_dimension": {
            "default": 3,
            "type": "integer"
          },
          "input_regex": {
            "default": ".*nii\\.gz",
            "type": "string"
          },
          "input_tags": {
            "default": "",
            "type": "string"
          },
          "number_of_modalities": {
            "default": 1,
            "type": "integer"
          },
          "rigid_alignment_to_SST": {
            "default": 0,
            "type": "integer"
          },
          "rigid_template_update_component": {
            "default": 0,
            "type": "integer"
          }
        },
        "required": [
          "debug",
          "image_dimension",
          "atropos_iteration",
          "denoise_anatomical_image",
          "number_of_modalities",
          "rigid_template_update_component",
          "rigid_alignment_to_SST",
          "input_regex",
          "input_tags",
          "atlases_template"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "api-key": {
            "type": "object"
          },
          "registered_predefined_atlases": {
            "properties": {},
            "type": "object"
          }
        },
        "required": [
          "api-key"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for ANTs DBM Longitudinal",
    "type": "object"
  }
}
