{
  "exchange": {
    "git-commit": "6c52031a5e759600e8c3cb60b8901900d3daf2df",
    "rootfs-hash": "sha256:22f58b8b0b5c1bf0772089bf4decb9276608ff0a51dd5c7ae5a677473b6df29e",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-fsl-sienax:1.0.2_5.0"
  },
  "gear": {
    "author": "Analysis Group, FMRIB, Oxford, UK.",
    "cite": "S.M. Smith, Y. Zhang, M. Jenkinson, J. Chen, P.M. Matthews, A. Federico, and N. De Stefano. Accurate, robust and automated longitudinal and cross-sectional brain change analysis. NeuroImage, 17(1):479-489, 2002.  S.M. Smith, M. Jenkinson, M.W. Woolrich, C.F. Beckmann, T.E.J. Behrens, H. Johansen-Berg, P.R. Bannister, M. De Luca, I. Drobnjak, D.E. Flitney, R. Niazy, J. Saunders, J. Vickers, Y. Zhang, N. De Stefano, J.M. Brady, and P.M. Matthews. Advances in functional and structural MR image analysis and implementation as FSL. NeuroImage, 23(S1):208-219, 2004.",
    "command": "python3 run.py",
    "config": {
      "BET": {
        "default": "",
        "description": "bet options",
        "id": "-B",
        "type": "string"
      },
      "BOTTOM": {
        "default": "",
        "description": "ignore from b (mm) upwards in MNI152/Talairach space [default=false]",
        "id": "-b",
        "type": "string"
      },
      "DEBUG": {
        "default": false,
        "description": "debug (don't delete intermediate files) [default=false]",
        "id": "-d",
        "type": "boolean"
      },
      "REGIONAL": {
        "default": false,
        "description": "tell SIENAX to estimate \"regional\" volumes as well as global; this produces peripheral cortex GM volume (3-class segmentation only) and ventricular CSF volume [default=false]",
        "id": "-r",
        "type": "boolean"
      },
      "SEG": {
        "default": false,
        "description": "two-class segmentation (don't segment grey and white matter separately) - use this if there is poor grey/white contrast [default=false]",
        "id": "-2",
        "type": "boolean"
      },
      "S_FAST": {
        "default": "",
        "description": "if you want to change the segmentation defaults, put FAST options inside double-quotes after using the -S flag. For example, to increase the number of segmentation iterations use: -S \"-i 20\"",
        "id": "-S",
        "type": "string"
      },
      "T2": {
        "default": false,
        "description": "tell FAST that the input images are T2-weighted and not T1 [default=false]",
        "id": "-t2",
        "type": "boolean"
      },
      "TOP": {
        "default": "",
        "description": "ignore from t (mm) upwards in MNI152/Talairach space [default=false]",
        "id": "-t",
        "type": "string"
      }
    },
    "custom": {
      "flywheel": {
        "classification": {
          "function": [
            "Image Processing - Structural"
          ],
          "modality": [
            "MR"
          ],
          "organ": [
            "Brain"
          ],
          "species": [
            "Animal",
            "Human"
          ],
          "therapeutic_area": [
            "Neurology",
            "Psychiatry/Psychology"
          ]
        },
        "suite": "Image Processing"
      },
      "gear-builder": {
        "category": "analysis",
        "image": "flywheel/fsl-sienax:1.0.2_5.0"
      }
    },
    "description": "FSL's SIENAX. Sienax estimates total brain tissue volume, from a single image, normalised for skull size. It calls a series of FSL programs: It first strips non-brain tissue, and then uses the brain and skull images to estimate the scaling between the subject's image and standard space. It then runs tissue segmentation to estimate the volume of brain tissue, and multiplies this by the estimated scaling factor, to reduce head-size-related variability between subjects. Inputs should be structural image (T1-weighted, T2-weighted, PD, etc) where the in-plane resolution is better than 2mm (ideally 1mm). Outputs consist of an archive containing the results of the analysis, as well as an HTML report summarizing the analysis findings. LICENSING NOTE: FSL software are owned by Oxford University Innovation and license is required for any commercial applications. For commercial licence please contact fsl@innovation.ox.ac.uk. For academic use, an academic license is required which is available by registering on the FSL website. Any use of the software requires that the user obtain the appropriate license. See https://fsl.fmrib.ox.ac.uk/fsldownloads_registration for more information.",
    "environment": {
      "FSLBROWSER": "/etc/alternatives/x-www-browser",
      "FSLDIR": "/usr/share/fsl/5.0",
      "FSLLOCKDIR": "",
      "FSLMACHINELIST": "",
      "FSLMULTIFILEQUIT": "TRUE",
      "FSLOUTPUTTYPE": "NIFTI_GZ",
      "FSLREMOTECALL": "",
      "FSLTCLSH": "/usr/bin/tclsh",
      "FSLWISH": "/usr/bin/wish",
      "LD_LIBRARY_PATH": "/usr/lib/fsl/5.0",
      "PATH": "/usr/lib/fsl/5.0:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
      "POSSUMDIR": "/usr/share/fsl/5.0"
    },
    "inputs": {
      "NIFTI": {
        "base": "file",
        "description": "Structural NIfTI file to be processed.",
        "optional": false,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "api-key": {
        "base": "api-key",
        "read-only": true
      },
      "lesion_mask": {
        "base": "file",
        "description": "an optional lesion (or lesion+CSF) mask to remove incorrectly labelled \"grey matter\" voxels",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      }
    },
    "label": "FSL: SIENAX - Brain tissue volume, normalised for subject head size",
    "license": "Other",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "fsl-sienax",
    "source": "https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/SIENA",
    "url": "https://github.com/flywheel-apps/fsl-siena-sienax/tree/master/fsl-sienax",
    "version": "1.0.2_5.0"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "BET": {
            "default": "",
            "id": "-B",
            "type": "string"
          },
          "BOTTOM": {
            "default": "",
            "id": "-b",
            "type": "string"
          },
          "DEBUG": {
            "default": false,
            "id": "-d",
            "type": "boolean"
          },
          "REGIONAL": {
            "default": false,
            "id": "-r",
            "type": "boolean"
          },
          "SEG": {
            "default": false,
            "id": "-2",
            "type": "boolean"
          },
          "S_FAST": {
            "default": "",
            "id": "-S",
            "type": "string"
          },
          "T2": {
            "default": false,
            "id": "-t2",
            "type": "boolean"
          },
          "TOP": {
            "default": "",
            "id": "-t",
            "type": "string"
          }
        },
        "required": [
          "BET",
          "BOTTOM",
          "DEBUG",
          "SEG",
          "S_FAST",
          "T2",
          "TOP",
          "REGIONAL"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "NIFTI": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "api-key": {
            "type": "object"
          },
          "lesion_mask": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          }
        },
        "required": [
          "api-key",
          "NIFTI"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for FSL: SIENAX - Brain tissue volume, normalised for subject head size",
    "type": "object"
  }
}
