{
  "exchange": {
    "git-commit": "6a9ac834ca05df4e4e7fa689eb5678aa1feb0d89",
    "rootfs-hash": "sha256:35dfd2097d44cbddab3da314d0580933f6bd521ac6d80d663e9ccd972756d411",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-rtp2-preproc:0.1.2_3.0.4"
  },
  "gear": {
    "author": "flywheel",
    "cite": "Lerma-Usabiaga, G., Liu, M., Paz-Alonso, P. M. & Wandell, B. A. Reproducible Tract Profiles 2 (RTP2) suite, from diffusion MRI acquisition to clinical practice and research. Sci. Rep. 13, 1-13 (2023)",
    "command": "python run.py",
    "config": {
      "anatalign": {
        "default": true,
        "description": "Align dwi data with anatomy",
        "type": "boolean"
      },
      "ants_dwi2anat_options": {
        "default": "-d 3 -t r",
        "description": "Options for registering the dwi files to the anatomy using antsRegistrationSyN.sh. The COMPULSORY antsRegistrationSyN options -f (Fixed image(s) or source image(s) or reference image(s)), -m (Moving image(s) or target image(s)), and -o (OutputPrefix: A prefix that is prepended to all output files.), will be set by the gear based on the gear inputs. The COMPULSORY antsRegistrationSyN option -d (ImageDimension: 2 or 3 (for 2 or 3 dimensional registration of single volume)) will be read from this string. If an option -d 2 or -d 3 is not found in this string, the gear will stop. The optional arguments will be checked too, and if they don't exist or the options are not ok according to the ANTs manual, the gear will stop too. The manual was taken and will be updated according to the Usage section in: https://github.com/ANTsX/ANTs/blob/master/Scripts/antsRegistrationSyN.sh. [default=-d 3 -t r]",
        "type": "string"
      },
      "ants_qmap2anat_options": {
        "default": "-d 3 -t r",
        "description": "Options for registering the optional qMAP files to the anatomy using antsRegistrationSyN.sh. See the description of ants_dwi2anat_options for more details.",
        "type": "string"
      },
      "antsb": {
        "default": "[150,3]",
        "description": "b params for ANTs (when called from dwibiascorrect ants, it goes to N4BiasFieldCorrection option -b). It is a comma-separated pair of values enclosed by square brackets. The first parameter corresponds to initial mesh resolution in mm, the second one corresponds to spline order [initial mesh resolution in mm, spline order]. This value is optimised for human adult data and needs to be adjusted for rodent data. --bspline-fitting [splineDistance,<splineOrder=3>] [initialMeshResolution,<splineOrder=3>] These options describe the b-spline fitting parameters. The initial b-spline mesh at the coarsest resolution is specified either as the number of elements in each dimension, e.g. 2x2x3 for 3-D images, or it can be specified as a single scalar parameter which describes the isotropic sizing of the mesh elements. The latter option is typically preferred. For each subsequent level, the spline distance decreases in half, or equivalently, the number of mesh elements doubles Cubic splines (order = 3) are typically used",
        "type": "string"
      },
      "antsc": {
        "default": "[200x200,1e-6]",
        "description": "c params for ANTs (when called from dwibiascorrect ants, it goes to N4BiasFieldCorrection option -c). It is a comma-separated pair of values enclosed by square brackets. The first parameter corresponds to numberOfIterations, the second one to convergenceThreshold. --convergence [<numberOfIterations=50x50x50x50>,<convergenceThreshold=0.0>] Convergence is determined by calculating the coefficient of variation between subsequent iterations. When this value is less than the specified threshold from the previous iteration or the maximum number of iterations is exceeded the program terminates. Multiple resolutions can be specified by using 'x' between the number of iterations at each resolution, e.g. 100x50x50.",
        "type": "string"
      },
      "antss": {
        "default": "2",
        "description": "s param for ANTs (when called from dwibiascorrect ants, it goes N4BiasFieldCorrection option -s). It is an integer, corresponding to the shrink-factor applied to spatial dimensions. It lessens computation time by resampling the input image. Shrink factors <= 4 are commonly used",
        "type": "string"
      },
      "bias": {
        "default": false,
        "description": "Compute bias correction with ANTs on dwi data",
        "type": "boolean"
      },
      "bias_method": {
        "default": "ants",
        "description": "ants or fsl option for dwibiascorrect: performs B1 field inhomogeneity correction for a DWI volume series",
        "enum": [
          "ants",
          "fsl"
        ],
        "type": "string"
      },
      "degibbs": {
        "default": false,
        "description": "Perform Gibbs ringing correction",
        "type": "boolean"
      },
      "denoise": {
        "default": false,
        "description": "Denoise data using PCA",
        "type": "boolean"
      },
      "doreslice": {
        "default": false,
        "description": "Do reslicing. If set to true, it will reslice the DWI data to the value set in the configuration option 'reslice'. If anatalign is set to True as well, then ",
        "type": "boolean"
      },
      "eddy": {
        "default": true,
        "description": "Perform eddy current correction. If inverted phase encoding direction files are found, eddy will be done as part of topup.",
        "type": "boolean"
      },
      "eddy_data_is_shelled": {
        "default": true,
        "description": "At the moment eddy works for single- or multi-shell diffusion data, i.e. it doesn't work for DSI data. In order to ensure that the data is shelled, eddy checks it and only proceeds if that's the case. The checking is performed through a set of heuristics such as i) how many shells are there? ii) what are the absolute numbers of directions for each shell? iii) what are the relative numbers of directions for each shell? etc. However, some popular schemes (e.g., if you acquire a mini shell with low b-value and few directions) get caught in this test, even though eddy works perfectly well on the data. eddy_data_is_shelled, if set, will bypass any checking and eddy will proceed as if data was shelled. Please be aware that if you have to use this flag you may be in untested territory and that it is a good idea to check your data extra carefully after having run eddy on it",
        "type": "boolean"
      },
      "eddy_niter": {
        "default": 5,
        "description": "eddy does not check for convergence. Instead, it runs a fixed number of iterations given by this parameter. If, on visual inspection, one finds residual movement or EC-induced distortions it is possible that eddy has not fully converged. In that case you can increase the number of iterations. See FSL Eddy User Guide for more details [default=5]",
        "type": "number"
      },
      "eddy_repol": {
        "default": true,
        "description": "When set, this flag instructs eddy to remove any slices deemed as outliers and replace them with predictions made by the Gaussian Process. Exactly what constitutes an outlier is affected by the parameters --ol_nstd, --ol_nvox, --ol_type, --ol_pos and --ol_sqr. If the defaults are used for all those parameters an outlier is defined as a slice whose average intensity is at least four standard deviations lower than the expected intensity, where the expectation is given by the Gaussian Process prediction. The default is to not do outlier replacement since we don't want to risk people using it unawares. However, our experience and tests indicate that it is always a good idea to use --repol. See FSL Eddy User Guide for more details. [default=linear]",
        "type": "boolean"
      },
      "eddy_slm": {
        "default": "linear",
        "description": "Second level model that specifies the mathematical form for how the diffusion gradients cause eddy currents. For high quality data with 60 directions, or more, sampled on the whole sphere we have not found any advantage of performing second level modeling. Hence, our recommendation for such data is to use none. If the data has few directions and/or has not been sampled on the whole sphere it is better to use linear [default=linear]",
        "type": "string"
      },
      "norm": {
        "default": false,
        "description": "Perform intensity normalization of dwi data",
        "type": "boolean"
      },
      "nval": {
        "default": 1000,
        "description": "Normalize the intensity of the FA white matter mask to this number",
        "type": "number"
      },
      "pe_dir": {
        "default": "",
        "description": "Phase Encoding Direction, optional field only necessary if eddy = true. This field will be automatically obtained from the json sidecar of the file. In old datasets, if the json file is not found, it will try to read it from here. By default the field is empty. The only acceptable values are AP, PA, LR, RL, IS, SI. If the values are not in the json sidecar file or any of these six values are not set in this field, the container will stop.",
        "type": "string"
      },
      "reslice": {
        "default": 1,
        "description": "Optional. By default reslicing is not done. If you want it done, set doreslice=true and set a number (e.g., 1) here to reslice the dwi data to this isotropic voxel size (in mm).",
        "type": "number"
      },
      "ricn": {
        "default": false,
        "description": "Perform Rician background noise removal",
        "type": "boolean"
      },
      "save_extra_output": {
        "default": false,
        "description": "Save all the intermediate files (both .mif and .nii.gz), for QC and debugging",
        "type": "boolean"
      },
      "topup_lambda": {
        "default": "0.005,0.001,0.0001,0.000015,0.000005,0.0000005,0.00000005,0.0000000005,0.00000000001",
        "description": "parameter lambda in topup. It sets the relative weight between sum-of-squared differences and regularization for each level [default=0.005,0.001,0.0001,0.000015,0.000005,0.0000005,0.00000005,0.0000000005,0.00000000001]. See FSL Topup User Guide for more details",
        "type": "string"
      }
    },
    "custom": {
      "flywheel": {
        "show-job": true,
        "suite": "Image Processing - Diffusion"
      },
      "gear-builder": {
        "category": "analysis",
        "image": "flywheel/rtp2-preproc:0.1.2_3.0.4"
      },
      "license": {
        "dependencies": [
          {
            "name": "Other",
            "url": "https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Licence"
          },
          {
            "name": "Other",
            "url": "https://github.com/ANTsX/ANTs/blob/v2.3.4/COPYING.txt"
          },
          {
            "name": "Other",
            "url": "https://surfer.nmr.mgh.harvard.edu/fswiki/License"
          }
        ],
        "main": {
          "name": "MIT"
        }
      }
    },
    "description": "RTP-preproc runs the MRtrix3 preprocessing pipeline, based on FSL's topup when the optional inverse phase encoded data are provided, otherwise the pipeline uses FSL's eddy tool. The pipeline can also perform de-noising, reslicing, and alignment to an anatomical image. Required inputs are diffusion NIfTI, BVEC, BVAL, and Anatomical (ACPC aligned) NIfTI.  LICENSING NOTE: FSL software are owned by Oxford University Innovation and license is required for any commercial applications. For commercial licence please contact fsl@innovation.ox.ac.uk. For academic use, an academic license is required which is available by registering on the FSL website. Any use of the software requires that the user obtain the appropriate license. See https://fsl.fmrib.ox.ac.uk/fsldownloads_registration for more information.",
    "environment": {
      "ANTSPATH": "/opt/ants/bin",
      "ARTHOME": "/opt/art",
      "DISPLAY": ":50.0",
      "FLYWHEEL": "/flywheel/v0",
      "FREESURFER_HOME": "/opt/freesurfer",
      "FSLDIR": "/opt/fsl",
      "FSLMULTIFILEQUIT": "TRUE",
      "FSLOUTPUTTYPE": "NIFTI_GZ",
      "FSLTCLSH": "/opt/fsl/bin/fsltclsh",
      "FSLWISH": "/opt/fsl/bin/fslwish",
      "FS_LICENSE": "/opt/freesurfer/license.txt",
      "GPG_KEY": "E3FF2839C048B25C084DEBE9B26995E310250568",
      "LANG": "C.UTF-8",
      "LD_LIBRARY_PATH": "/opt/fsl/lib:",
      "PATH": "/opt/mrtrix3/bin:/opt/ants/bin:/opt/art/bin:/opt/fsl/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
      "PWD": "/flywheel/v0",
      "PYTHON_GET_PIP_SHA256": "1e501cf004eac1b7eb1f97266d28f995ae835d30250bec7f8850562703067dc6",
      "PYTHON_GET_PIP_URL": "https://github.com/pypa/get-pip/raw/66030fa03382b4914d4c4d0896961a0bdeeeb274/public/get-pip.py",
      "PYTHON_PIP_VERSION": "22.0.4",
      "PYTHON_SETUPTOOLS_VERSION": "58.1.0",
      "PYTHON_VERSION": "3.9.15",
      "QT_QPA_PLATFORM": "xcb"
    },
    "flywheel": "0",
    "inputs": {
      "ANAT": {
        "base": "file",
        "description": "Freesurferator anatomical T1w NIfTI image.",
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "BVAL": {
        "base": "file",
        "description": "BVAL file.",
        "type": {
          "enum": [
            "bval"
          ]
        }
      },
      "BVEC": {
        "base": "file",
        "description": "BVEC file.",
        "type": {
          "enum": [
            "bvec"
          ]
        }
      },
      "DIFF": {
        "base": "file",
        "description": "Diffusion NIfTI image.",
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "FSMASK": {
        "base": "file",
        "description": "Freesurferator brain mask",
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "QMAP": {
        "base": "file",
        "description": "Quantitative map to be registered to the anatomical image. This map can be used to obtain tract and ROI metrics in RTP2-pipeline. Even though this option was created for qMRI maps, it really can be used with any scalar map with a value per voxel.",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "RBVC": {
        "base": "file",
        "description": "Optional reverse phase encoded (rpe) BVEC file.",
        "optional": true,
        "type": {
          "enum": [
            "bvec"
          ]
        }
      },
      "RBVL": {
        "base": "file",
        "description": "Optional reverse phase encoded (rpe) BVAL file.",
        "optional": true,
        "type": {
          "enum": [
            "bval"
          ]
        }
      },
      "RDIF": {
        "base": "file",
        "description": "Optional reverse phase encoded (rpe) diffusion NIfTI image.",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "api-key": {
        "base": "api-key",
        "read-only": true
      }
    },
    "label": "RTP2 Preprocessing Pipeline",
    "license": "Other",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "rtp2-preproc",
    "source": "https://gitlab.com/flywheel-io/scientific-solutions/gears/rtp2-preproc",
    "url": "https://gitlab.com/flywheel-io/scientific-solutions/gears/rtp2-preproc",
    "version": "0.1.2_3.0.4"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "anatalign": {
            "default": true,
            "type": "boolean"
          },
          "ants_dwi2anat_options": {
            "default": "-d 3 -t r",
            "type": "string"
          },
          "ants_qmap2anat_options": {
            "default": "-d 3 -t r",
            "type": "string"
          },
          "antsb": {
            "default": "[150,3]",
            "type": "string"
          },
          "antsc": {
            "default": "[200x200,1e-6]",
            "type": "string"
          },
          "antss": {
            "default": "2",
            "type": "string"
          },
          "bias": {
            "default": false,
            "type": "boolean"
          },
          "bias_method": {
            "default": "ants",
            "enum": [
              "ants",
              "fsl"
            ],
            "type": "string"
          },
          "degibbs": {
            "default": false,
            "type": "boolean"
          },
          "denoise": {
            "default": false,
            "type": "boolean"
          },
          "doreslice": {
            "default": false,
            "type": "boolean"
          },
          "eddy": {
            "default": true,
            "type": "boolean"
          },
          "eddy_data_is_shelled": {
            "default": true,
            "type": "boolean"
          },
          "eddy_niter": {
            "default": 5,
            "type": "number"
          },
          "eddy_repol": {
            "default": true,
            "type": "boolean"
          },
          "eddy_slm": {
            "default": "linear",
            "type": "string"
          },
          "norm": {
            "default": false,
            "type": "boolean"
          },
          "nval": {
            "default": 1000,
            "type": "number"
          },
          "pe_dir": {
            "default": "",
            "type": "string"
          },
          "reslice": {
            "default": 1,
            "type": "number"
          },
          "ricn": {
            "default": false,
            "type": "boolean"
          },
          "save_extra_output": {
            "default": false,
            "type": "boolean"
          },
          "topup_lambda": {
            "default": "0.005,0.001,0.0001,0.000015,0.000005,0.0000005,0.00000005,0.0000000005,0.00000000001",
            "type": "string"
          }
        },
        "required": [
          "anatalign",
          "ants_dwi2anat_options",
          "ants_qmap2anat_options",
          "antsb",
          "antsc",
          "antss",
          "bias",
          "bias_method",
          "degibbs",
          "denoise",
          "doreslice",
          "eddy",
          "eddy_data_is_shelled",
          "eddy_niter",
          "eddy_repol",
          "eddy_slm",
          "norm",
          "nval",
          "pe_dir",
          "reslice",
          "ricn",
          "save_extra_output",
          "topup_lambda"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "ANAT": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "BVAL": {
            "properties": {
              "type": {
                "enum": [
                  "bval"
                ]
              }
            },
            "type": "object"
          },
          "BVEC": {
            "properties": {
              "type": {
                "enum": [
                  "bvec"
                ]
              }
            },
            "type": "object"
          },
          "DIFF": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "FSMASK": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "QMAP": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "RBVC": {
            "properties": {
              "type": {
                "enum": [
                  "bvec"
                ]
              }
            },
            "type": "object"
          },
          "RBVL": {
            "properties": {
              "type": {
                "enum": [
                  "bval"
                ]
              }
            },
            "type": "object"
          },
          "RDIF": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "api-key": {
            "type": "object"
          }
        },
        "required": [
          "ANAT",
          "BVAL",
          "BVEC",
          "DIFF",
          "FSMASK",
          "api-key"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for RTP2 Preprocessing Pipeline",
    "type": "object"
  }
}
