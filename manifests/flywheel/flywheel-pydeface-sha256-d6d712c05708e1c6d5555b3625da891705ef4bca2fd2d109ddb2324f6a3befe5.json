{
  "exchange": {
    "git-commit": "b16370f312e6049c459b64e20843415fdf465e7e",
    "rootfs-hash": "sha256:d6d712c05708e1c6d5555b3625da891705ef4bca2fd2d109ddb2324f6a3befe5",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-pydeface:0.3.0_2.0.2"
  },
  "gear": {
    "author": "Flywheel",
    "cite": "PyDeface doi: 10.5281/zenodo.6856482",
    "command": "python run.py",
    "config": {
      "copy_input_metadata_to_output": {
        "default": true,
        "description": "Copy the input file's metadata to the output file. If overwrite_input=false, tags from the input file are not transferred, only info, modality, and classification.",
        "type": "boolean"
      },
      "cost": {
        "default": "mutualinfo",
        "description": "FSL-FLIRT cost function",
        "enum": [
          "mutualinfo",
          "corratio",
          "normcorr",
          "normmi",
          "leastsq",
          "labeldiff",
          "bbr"
        ],
        "type": "string"
      },
      "debug": {
        "default": false,
        "description": "Log debug messages",
        "type": "boolean"
      },
      "nocleanup": {
        "default": false,
        "description": "Do not cleanup temporary files. Off by default.",
        "type": "boolean"
      },
      "overwrite_input": {
        "default": false,
        "description": "Overwrite original file with defaced version",
        "type": "boolean"
      },
      "tag": {
        "default": "pydeface",
        "description": "The tag to be added to files upon run completion",
        "type": "string"
      },
      "tag_input": {
        "default": false,
        "description": "If true, also tag the input file with the tag specified in the 'tag' field. If false, only the output file is tagged. If overwrite_input=false, this option is ignored (since it's redundant).",
        "type": "boolean"
      },
      "verbose": {
        "default": false,
        "description": "Show additional status prints. Off by default.",
        "type": "boolean"
      }
    },
    "custom": {
      "flywheel": {
        "classification": {
          "function": [
            "Image Processing - Structural",
            "Utility"
          ],
          "modality": [
            "MR"
          ],
          "organ": [
            "Brain"
          ],
          "species": [
            "Human"
          ],
          "therapeutic_area": [
            "Neurology",
            "Oncology",
            "Psychiatry/Psychology"
          ]
        },
        "show-job": true,
        "suite": "Image Processing"
      },
      "gear-builder": {
        "category": "converter",
        "image": "flywheel/pydeface:0.3.0_2.0.2"
      },
      "license": {
        "main": {
          "name": "MIT",
          "url": "https://github.com/poldracklab/pydeface/blob/v2.0.2/LICENSE.txt"
        }
      }
    },
    "description": "Flywheel gear for the implementation of Poldrack Lab's PyDeface to remove facial structures from MRI images.",
    "environment": {
      "BUILD_TIME": "2023-03-20T11:43:06Z",
      "COMMIT_REF": "main",
      "COMMIT_SHA": "fa0da4dd",
      "CONDA_DEFAULT_ENV": "neuro",
      "CONDA_DIR": "/opt/conda",
      "CONDA_EXE": "/opt/conda/bin/conda",
      "CONDA_PREFIX": "/opt/conda/envs/neuro",
      "CONDA_PROMPT_MODIFIER": "(neuro) ",
      "CONDA_PYTHON_EXE": "/opt/conda/bin/python",
      "CONDA_SHLVL": "1",
      "DEBIAN_FRONTEND": "noninteractive",
      "EDITOR": "micro",
      "FLYWHEEL": "/flywheel/v0",
      "FSLBROWSER": "/etc/alternatives/x-www-browser",
      "FSLDIR": "/usr/share/fsl/6.0",
      "FSLGECUDAQ": "cuda.q",
      "FSLLOCKDIR": "",
      "FSLMACHINELIST": "",
      "FSLMULTIFILEQUIT": "TRUE",
      "FSLOUTPUTTYPE": "NIFTI_GZ",
      "FSLREMOTECALL": "",
      "FSLTCLSH": "/usr/bin/tclsh",
      "FSLWISH": "/usr/bin/wish",
      "FSL_DIR": "/usr/share/fsl/6.0",
      "GPG_KEY": "A035C8C19219BA821ECEA86B64E628F8D684696D",
      "HOME": "/root",
      "HOSTNAME": "d8012530f084",
      "LANG": "C.UTF-8",
      "LC_ALL": "C.UTF-8",
      "LD_LIBRARY_PATH": "/usr/share/fsl/6.0/lib:",
      "ND_ENTRYPOINT": "/neurodocker/startup.sh",
      "NEURODEB_KEY": "http://neuro.debian.net/_static/neuro.debian.net.asc",
      "PATH": "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/share/fsl/6.0/bin",
      "POSSUMDIR": "/usr/share/fsl/6.0",
      "PROMETHEUS_MULTIPROC_DIR": "/var/metrics",
      "PWD": "/flywheel/v0",
      "PYDEFACE_VERSION": "v2.0.2",
      "PYSITE": "/usr/local/lib/python3.11/site-packages",
      "PYTHON_GET_PIP_SHA256": "394be00f13fa1b9aaa47e911bdb59a09c3b2986472130f30aa0bfaf7f3980637",
      "PYTHON_GET_PIP_URL": "https://github.com/pypa/get-pip/raw/d5cb0afaf23b8520f1bbcfed521017b4a95f5c01/public/get-pip.py",
      "PYTHON_PIP_VERSION": "22.3.1",
      "PYTHON_SETUPTOOLS_VERSION": "65.5.1",
      "PYTHON_VERSION": "3.11.2",
      "SETUPTOOLS_USE_DISTUTILS": "stdlib",
      "SHLVL": "1",
      "TERM": "xterm",
      "_": "/opt/conda/envs/neuro/bin/python",
      "_CE_CONDA": "",
      "_CE_M": ""
    },
    "inputs": {
      "api-key": {
        "base": "api-key",
        "read-only": false
      },
      "applyto": {
        "base": "file",
        "description": "Optional, Apply the created facemask to another image.",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "facemask": {
        "base": "file",
        "description": "Optional face mask image that will be used instead of the default.",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "infile": {
        "base": "file",
        "description": "input nifti",
        "optional": false,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "template": {
        "base": "file",
        "description": "Optional template image that will be used as the registration target instead of the default.",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      }
    },
    "label": "Pydeface",
    "license": "Other",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "pydeface",
    "source": "https://gitlab.com/flywheel-io/scientific-solutions/gears/pydeface",
    "url": "https://github.com/poldracklab/pydeface",
    "version": "0.3.0_2.0.2"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "copy_input_metadata_to_output": {
            "default": true,
            "type": "boolean"
          },
          "cost": {
            "default": "mutualinfo",
            "enum": [
              "mutualinfo",
              "corratio",
              "normcorr",
              "normmi",
              "leastsq",
              "labeldiff",
              "bbr"
            ],
            "type": "string"
          },
          "debug": {
            "default": false,
            "type": "boolean"
          },
          "nocleanup": {
            "default": false,
            "type": "boolean"
          },
          "overwrite_input": {
            "default": false,
            "type": "boolean"
          },
          "tag": {
            "default": "pydeface",
            "type": "string"
          },
          "tag_input": {
            "default": false,
            "type": "boolean"
          },
          "verbose": {
            "default": false,
            "type": "boolean"
          }
        },
        "required": [
          "copy_input_metadata_to_output",
          "cost",
          "debug",
          "nocleanup",
          "overwrite_input",
          "tag",
          "tag_input",
          "verbose"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "api-key": {
            "type": "object"
          },
          "applyto": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "facemask": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "infile": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "template": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          }
        },
        "required": [
          "api-key",
          "infile"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for Pydeface",
    "type": "object"
  }
}
