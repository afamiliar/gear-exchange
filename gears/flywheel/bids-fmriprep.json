{
  "author": "Poldrack lab, Stanford University",
  "cite": "see https://fmriprep.org/en/stable/index.html#citation",
  "command": "/opt/flypy/bin/python run.py",
  "config": {
    "bids_app_command": {
      "default": "",
      "description": "OPTIONAL. The gear will run the algorithm defaults, if no command is supplied. See https://fmriprep.org/en/stable/usage.html#command-line-arguments for options. e.g., fmriprep bids_dir output_dir participant [arg1 [arg2 ...]]",
      "type": "string"
    },
    "debug": {
      "default": false,
      "description": "Log debug messages for Flywheel. Please add elevated fMRIPrep debug levels to the bids_app_command as an arg. ('i.e.,[--debug {compcor,fieldmaps,pdb,all} [{compcor,fieldmaps,pdb,all} ...]]",
      "type": "boolean"
    },
    "gear-FREESURFER_LICENSE": {
      "default": "",
      "description": "Text from license file generated during FreeSurfer registration. *Entries should be space separated*",
      "type": "string"
    },
    "gear-dry-run": {
      "default": false,
      "description": "Do everything Flywheel-related except actually execute BIDS App command. Different from passing '--dry-run' in the BIDS App command.",
      "type": "boolean"
    },
    "gear-expose-all-outputs": {
      "default": false,
      "description": "Keep ALL the extra output files that are created during the run in addition to the normal, zipped output. Note: This option may cause a gear failure because there are too many files for the engine.",
      "type": "boolean"
    },
    "gear-intermediate-files": {
      "default": "",
      "description": "Space separated list of FILES to retain from the intermediate work directory.",
      "type": "string"
    },
    "gear-intermediate-folders": {
      "default": "",
      "description": "Space separated list of FOLDERS to retain from the intermediate work directory.",
      "type": "string"
    },
    "gear-keep-fsaverage": {
      "default": false,
      "description": "Keep freesurfer/fsaverage* directories in output.  These are copied from the freesurfer installation.  Default is to delete them",
      "type": "boolean"
    },
    "gear-post-processing-only": {
      "default": false,
      "description": "REQUIRES archive file. Gear will skip the BIDS algorithm and go straight to generating the HTML reports and processing metadata.",
      "type": "boolean"
    },
    "gear-save-intermediate-output": {
      "default": false,
      "description": "Gear will save ALL intermediate output into fmriprep_work.zip",
      "type": "boolean"
    }
  },
  "custom": {
    "analysis-level": "participant",
    "bids-app-binary": "fmriprep",
    "bids-app-data-types": [
      "anat",
      "func",
      "fmap"
    ],
    "flywheel": {
      "classification": {
        "function": [
          "Image Processing - Functional"
        ],
        "modality": [
          "MR"
        ],
        "organ": [
          "Brain"
        ],
        "species": [
          "Human"
        ],
        "therapeutic_area": [
          "Neurology",
          "Psychiatry/Psychology"
        ]
      },
      "show-job": true,
      "suite": "BIDS Apps"
    },
    "gear-builder": {
      "category": "analysis",
      "image": "flywheel/bids-fmriprep:1.5.1_24.0.0"
    },
    "license": {
      "dependencies": [
        {
          "name": "Other",
          "url": "https://fmriprep.org/en/stable/index.html#license"
        },
        {
          "name": "Other",
          "url": "https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Licence"
        },
        {
          "name": "Other",
          "url": "https://github.com/ANTsX/ANTs/blob/v2.2.0/COPYING.txt"
        },
        {
          "name": "Other",
          "url": "https://afni.nimh.nih.gov/pub/dist/doc/program_help/README.copyright.html"
        },
        {
          "name": "Other",
          "url": "https://github.com/freesurfer/freesurfer/blob/dev/LICENSE.txt"
        }
      ],
      "main": {
        "name": "BSD-3-Clause",
        "url": "https://github.com/poldracklab/fmriprep/blob/master/LICENSE"
      },
      "non-commercial-use-only": true
    },
    "main": {
      "name": "BSD-3-Clause",
      "url": "BIDS App URL"
    },
    "non-commercial-use-only": false
  },
  "description": "fMRIPrep 24.0.0 is a functional magnetic resonance imaging (fMRI) data preprocessing pipeline that is designed to provide an easily accessible, state-of-the-art interface that is robust to variations in scan acquisition protocols and that requires minimal user input, while providing easily interpretable and comprehensive error and output reporting. It performs basic processing steps (coregistration, normalization, unwarping, noise component extraction, segmentation, skullstripping etc.) providing outputs that can be easily submitted to a variety of group level analyses, including task-based or resting-state fMRI, graph theory measures, surface or volume-based statistics, etc.",
  "environment": {
    "AFNI_IMSAVE_WARNINGS": "NO",
    "AFNI_PLUGINPATH": "/opt/afni-latest",
    "CPATH": "/opt/conda/envs/fmriprep/include:",
    "DEBIAN_FRONTEND": "noninteractive",
    "FLYWHEEL": "/flywheel/v0",
    "FREESURFER_HOME": "/opt/freesurfer",
    "FSF_OUTPUT_FORMAT": "nii.gz",
    "FSLDIR": "/opt/conda/envs/fmriprep",
    "FSLGECUDAQ": "cuda.q",
    "FSLMULTIFILEQUIT": "TRUE",
    "FSLOUTPUTTYPE": "NIFTI_GZ",
    "FS_OVERRIDE": "0",
    "FUNCTIONALS_DIR": "/opt/freesurfer/sessions",
    "IS_DOCKER_8395080871": "1",
    "LANG": "C.UTF-8",
    "LC_ALL": "C.UTF-8",
    "LD_LIBRARY_PATH": "/opt/conda/envs/fmriprep/lib:/usr/lib/x86_64-linux-gnu:/opt/workbench/lib_linux64:",
    "LOCAL_DIR": "/opt/freesurfer/local",
    "MAMBA_ROOT_PREFIX": "/opt/conda",
    "MINC_BIN_DIR": "/opt/freesurfer/mni/bin",
    "MINC_LIB_DIR": "/opt/freesurfer/mni/lib",
    "MKL_NUM_THREADS": "1",
    "MNI_DATAPATH": "/opt/freesurfer/mni/data",
    "MNI_DIR": "/opt/freesurfer/mni",
    "MNI_PERL5LIB": "/opt/freesurfer/mni/lib/perl5/5.8.5",
    "OMP_NUM_THREADS": "1",
    "OS": "Linux",
    "PATH": "/opt/conda/envs/fmriprep/bin:/opt/workbench/bin_linux64:/opt/afni-latest:/opt/freesurfer/bin:/opt/freesurfer/tktools:/opt/freesurfer/mni/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
    "PERL5LIB": "/opt/freesurfer/mni/lib/perl5/5.8.5",
    "PWD": "/flywheel/v0",
    "PYTHONNOUSERSITE": "1",
    "SUBJECTS_DIR": "/opt/freesurfer/subjects"
  },
  "inputs": {
    "api-key": {
      "base": "api-key",
      "read-only": true
    },
    "bids-filter-file": {
      "base": "file",
      "description": "a JSON file describing custom BIDS input filters using PyBIDS.",
      "optional": true
    },
    "bidsignore": {
      "base": "file",
      "description": "A .bidsignore file to provide to the bids-validator that this gear runs before running the main command.",
      "optional": true
    },
    "derivatives": {
      "base": "file",
      "description": "Provide previously calculated fMRIPrep output results as a zip file.  This file will be unzipped and automatically become the base BIDS directory. In so doing, previous results will be used instead of re-calculating them.  This input is provided so that bids-fmriprep can be run incrementally as new data is acquired.",
      "optional": true
    },
    "freesurfer_license_file": {
      "base": "file",
      "description": "FreeSurfer license file, provided during registration with FreeSurfer. This file will by copied to the $FSHOME directory and used during execution of the Gear.",
      "optional": true
    },
    "fs-subjects-dir": {
      "base": "file",
      "description": "Zip file of existing FreeSurfer subject's directory to reuse.  If the output of FreeSurfer recon-all is provided to fMRIPrep, that output will be used rather than re-running recon-all.  Unzipping the file should produce a particular subject's directory which will be placed in the $FREESURFER_HOME/subjects directory.  The name of the directory must match the -subjid as passed to recon-all.  This version of fMRIPrep uses Freesurfer v6.0.1.",
      "optional": true
    },
    "previous_results": {
      "base": "file",
      "description": "Previous results that will be added to the BIDS directory.",
      "optional": true
    }
  },
  "label": "BIDS fMRIPrep: A Robust Preprocessing Pipeline for fMRI Data",
  "license": "BSD-3-Clause",
  "maintainer": "Flywheel <support@flywheel.io>",
  "name": "bids-fmriprep",
  "source": "https://github.com/nipreps/fmriprep",
  "url": "https://gitlab.com/flywheel-io/scientific-solutions/gears/bids-apps/bids-fmriprep",
  "version": "1.5.1_24.0.0"
}
